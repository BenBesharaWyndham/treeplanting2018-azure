<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTreesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('TI_Proposed_Plantings', function (Blueprint $table) {
            $table->increments('planting_id');
            $table->string('near_address');
            $table->string('notify_address');
            $table->string('latitude');
            $table->string('longitude');
            $table->string('locality');
            $table->string('tree_common');
            $table->string('tree_botanical');
            $table->string('tree_origin');
            $table->string('comments')->nullable();
            $table->string('financial_year')->nullable();
            $table->timestamp('date_created')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('date_updated')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
            $table->string('planted')->nullable();
            $table->string('planted_date')->nullable();
            $table->integer('tree_id')->nullable();
            $table->boolean('archived');
            $table->boolean('display');
            $table->string('device_id')->nullable();
            $table->datetime('inspection_date_obj')->nullable();
            $table->datetime('modified_date_obj')->nullable();
            $table->string('Geometry')->nullable();
            $table->string('Geometry_XLO')->nullable();
            $table->string('Geometry_YLO')->nullable();
            $table->string('Geometry_XHI')->nullable();
            $table->string('Geometry_YHI')->nullable();
            $table->string('near_propnum')->nullable();
            $table->string('username')->nullable();
            $table->string('assigned_to')->nullable();
            $table->string('reference')->nullable();
            $table->string('near_address_wcc')->nullable();
            $table->string('near_propnum_wcc')->nullable();
            $table->string('near_locality_wcc')->nullable();
            $table->string('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('TI_ProposedPlantings');
    }
}
