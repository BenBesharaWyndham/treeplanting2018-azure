<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
          'name' => 'Roy',
          'email' => 'xgao@wyndham.vic.gov.au',
          'password' => bcrypt('secret'),
          'role' => 1,
        ]);

        DB::table('users')->insert([
          'name' => 'Ben',
          'email' => 'xin.gao.au@gmail.com',
          'password' => bcrypt('secret'),
          'role' => 2,
        ]);

        DB::table('roles')->insert([
          'name' => 'Admin',
          'role_id' => 1
        ]);

        DB::table('roles')->insert([
          'name' => 'Contractor',
          'role_id' => 2
        ]);
    }
}
