<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\MapController;

class AuditPhotos extends Model
{
	protected $primary_key = 'file_id';
	protected $table = 'TP_Audit_Photos';
	
	public function getKeyName(){
    	return "file_id";
	}
}