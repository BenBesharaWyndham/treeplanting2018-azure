<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\MapController;

class TreeComments extends Model
{
	protected $primary_key = 'msg_id';
	protected $table = 'TP_comments';
	
	public function getKeyName(){
    	return "msg_id";
	}
}