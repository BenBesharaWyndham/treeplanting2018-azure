<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TI_ProposedPlantings extends Model
{
  protected $table = 'ti_proposed_plantings';
  protected $primaryKey = 'planting_id';

  const CREATED_AT = 'date_created';
  const UPDATED_AT = 'date_updated';

  protected $fillable = [
    'planting_id', 'near_address', 'notify_address', 'latitude', 'longitude', 'locality', 'tree_common', 'tree_botanical', 'tree_origin',
  ];
}
