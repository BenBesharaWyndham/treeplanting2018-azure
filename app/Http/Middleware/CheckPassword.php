<?php

namespace App\Http\Middleware;

use Closure;
use App\Http\User;
use Illuminate\Support\Facades\Auth;

class CheckPassword
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
	    if(Auth::check()){
		    if($request->user()->updated_at != $request->user()->created_at){
        		return $next($request);
			} else {
	        	return redirect('/password/change');
        	}
        } else return $next($request);
    }
}