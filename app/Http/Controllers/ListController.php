<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ProposedTree;
use App\TreeComments;

class ListController extends Controller
{
	function getListOfTrees() {
		//Get the data
		$trees = ProposedTree::leftJoin('TP_comments', 'TreePlantingSeason2018.ID1', 'TP_comments.tree_id')->select('TreePlantingSeason2018.*', 'TP_comments.*')->get();
		
		//Process the data
		$previousID = 0;
		$previousKey = 0;
		foreach($trees as $key => $tree){
			if($tree->message){
				$tree->message .= " - " . $tree->timestamp;
			}
			if($tree->ID1 == $previousID){
				$trees[$previousKey]->message .= "<br />" . $tree->message;
				unset($trees[$key]);
			} else {
				$previousID = $tree->ID1;
				$previousKey = $key;
			}
		}
		
		//Return the data
		$return['data'] = [];
		foreach($trees as $key => $tree){
			if(!$tree->status){
				$tree->status = "unchecked";
			}
			$treeInfo = [
				'ID' => $tree->ID1,
				'Address' => $tree->STREET_NAM,
				'Botanical Name' => $tree->New_BotNam,
				'Common Name' => $tree->New_ComNam,
				'Status' => $tree->status,
				'Date Updated' => $tree->status_changed,
				'User' => $tree->status_changed_user,
				'Comments' => $tree->Comments . "<br />" . $tree->message
			];
			array_push($return['data'], $treeInfo);
		}
		return json_encode($return);
	}
	
	function getListOfTreesInDateRange(Request $request) {
		$vals = explode("|", $request->req);
		$range = $vals[0];
		
		if($range == "week"){
			$date = date("Y-m-d H:i:s", strtotime("-7 days"));
		} else if($range == "month"){
			$date = date("Y-m-d H:i:s", strtotime("-30 days"));
		}
		
		$trees = ProposedTree::leftJoin('TP_comments', 'TreePlantingSeason2018.ID1', 'TP_comments.tree_id')->select('TreePlantingSeason2018.*', 'TP_comments.*')->where('status_changed', '>=', $date)->get();
		
		//Process the data
		$previousID = 0;
		$previousKey = 0;
		foreach($trees as $key => $tree){
			if($tree->message){
				$tree->message .= " - " . $tree->timestamp;
			}
			if($tree->ID1 == $previousID){
				$trees[$previousKey]->message .= "<br />" . $tree->message;
				unset($trees[$key]);
			} else {
				$previousID = $tree->ID1;
				$previousKey = $key;
			}
		}
		
		$return['data'] = [];
		foreach($trees as $key => $tree){
			if(!$tree->status){
				$tree->status = "unchecked";
			}
			$treeInfo = [
				'ID' => $tree->ID1,
				'Address' => $tree->STREET_NAM,
				'Botanical Name' => $tree->New_BotNam,
				'Common Name' => $tree->New_ComNam,
				'Status' => $tree->status,
				'Date Updated' => $tree->status_changed,
				'User' => $tree->status_changed_user,
				'Comments' => $tree->message
			];
			array_push($return['data'], $treeInfo);
		}
		return json_encode($return);
	}
	
	function getListOfUsers(){
		$users = ProposedTree::select("status_changed_user")->distinct()->get();
		return json_encode($users);
	}
	
	function showList() {
	  return view('treeList');
  	}
  
	function index() {
		
	}
}