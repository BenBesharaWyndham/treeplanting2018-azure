<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ProposedTree;
use App\TreeNames;
use App\TreeComments;
use App\AuditPhotos;

class MapController extends Controller
{

  function showMap() {
    if(!\Auth::guest())
      \Log::info("User is ".\Auth::user()->role()->get()[0]->role_name);
	  return view('treeInspect');
  }
  
  function inBounds($point, $boundingBox) {

    $pointLat = $point->lat;
    $pointLong = $point->lng;

    $boundsNElat = $boundingBox->boundsNElat;
    $boundsNElong = $boundingBox->boundsNElong;
    $boundsSWlat = $boundingBox->boundsSWlat;
    $boundsSWlong = $boundingBox->boundsSWlong;

    $eastBound = $pointLong < $boundsNElong;
    $westBound = $pointLong > $boundsSWlong;

    if ($boundsNElong < $boundsSWlong) {
      $inLong = $eastBound || $westBound;
    } else {
      $inLong = $eastBound && $westBound;
    }

    $inLat = $pointLat > $boundsSWlat && $pointLat < $boundsNElat;
    return $inLat && $inLong;
  }

  function getPointsWithinBounds($boundingBox) {

    $boundsNElat = $boundingBox['boundsNElat'];
    $boundsNElong = $boundingBox['boundsNElong'];
    $boundsSWlat = $boundingBox['boundsSWlat'];
    $boundsSWlong = $boundingBox['boundsSWlong'];

    $results = ProposedTree::where('LATITUDE', '<', $boundsNElat)->
    where('LATITUDE','>', $boundsSWlat)->
    where('LONGITUDE', '<', $boundsSWlong)->
    where('LONGITUDE', '>', $boundsNElong)->get();
	
    return $results;
  }

  function GetAllProposedTrees($request) {

    $vals = explode(",", $request->req);
    ini_set('precision', 17); //set precision to prevent casting losing number

    $params = [
      'boundsNElat' => floatval($vals[0]),
      'boundsSWlat' => floatval($vals[1]),
      'boundsNElong' => floatval($vals[2]),
      'boundsSWlong' => floatval($vals[3])
    ];

    $points = $this->getPointsWithinBounds($params);

    return $points;
  }
  
  function getNextTreeID() {
	  \Debugbar::disable();
	  $nextID = ProposedTree::count('ID1') + 1;
	  return $nextID;
  }
  
  function createNewMarker(Request $request) {
	  $vals = explode("|", $request->req);
	  
	  ProposedTree::insert([
	  	'ID1' => $vals[2],
	  	'LATITUDE' => $vals[0],
	  	'LONGITUDE' => $vals[1],
	  	'status' => "Unchecked",
	  	'PROP_ADD' => ""
	  ]);
  }
  
  function updateMarkerPosition(Request $request) {
	  $vals = explode(",", $request->req);
	  ini_set('precision', 17); //set precision to prevent casting losing number
	  $id = $vals[0];
	  $lat = $vals[1];
	  $lng = $vals[2];
	  
	  ProposedTree::where('ID1', $id)->update([
	  	'LATITUDE' => $lat,
	  	'LONGITUDE' => $lng
	  ]);
  }
  
  function updateTreeStatus(Request $request) {
	  $vals = explode("|", $request->req);
	  $id = $vals[0];
	  $status = $vals[1];
	  $user = $vals[2];
	  
	  ProposedTree::where('ID1', $id)->update([
		  'status' => $status,
		  'status_changed' => date("Y-m-d H:i:s"),
		  'status_changed_user' => $user
	  ]);
  }
  
  function getTreeNameArray() {
	  \Debugbar::disable();
	  return json_encode(TreeNames::get());
  }
  
  function getFuzzyTreeNameSearch(Request $request){
    \Debugbar::disable();
    $vals = explode("|", $request->req);
    $arr = TreeNames::where('tree_botanical', 'like', '%' + $vals[0] + '%')->
                      where('tree_common', 'like', '%' + $vals[1] + '%')->
                      get();
    return response()->json($arr);
  }

  function getTreeNameIDFromName(Request $request) {
	  \Debugbar::disable();
	  $vals = explode("|", $request->req);
	  if($vals[0] == "common") {
		  $treeID = TreeNames::where('tree_common', $vals[1])->get(["tree_type_id"]);
	  } else if($vals[0] == "botanical") {
		  $treeID = TreeNames::where('tree_botanical', $vals[1])->get(["tree_type_id"]);
	  }
	  if(isset(json_decode($treeID)[0])){
		  return json_decode($treeID)[0]->tree_type_id;
	  } else return 0;
  }
  
  function idToName($type, $id) {
	  \Debugbar::disable();

	  if($type == "common") {
		  $treeID = TreeNames::where('tree_type_id', $id)->select('tree_common as name')->get();
	  } else if($type == "botanical") {
		  $treeID = TreeNames::where('tree_type_id', $id)->select('tree_botanical as name')->get();
	  }
	  if(isset(json_decode($treeID)[0])){
		  return json_decode($treeID)[0]->name;
	  } else return 0;
  }
  
  function updateMarkerMetadata(Request $request) {
	  $vals = explode("|", $request->req);
	  ini_set('precision', 17); //set precision to prevent casting losing number
	  \Log::info($vals);
	  ProposedTree::where('ID1', $vals[0])->update([
		  'EX_BotNam' => $vals[1],
		  'EX_ComNam' => $vals[3],
		  'New_BotNam' => $this->idToName("botanical", $vals[2]),
		  'New_ComNam' => $this->idToName("common", $vals[4]),
		  'Comments' => $vals[5],
		  'PROP_ADD' => $vals[6]
	  ]);
  }
  
  function getMarkerMetadata(Request $request) {
	  $vals = explode("|", $request->req);
	  $id = $vals[0];
	  
	  $res = ProposedTree::where('ID1', $vals[0])->get();
	  \Log::info($res);
	  $return = array(
		  'id' => $res[0]->ID1,
		  'nearest_address' => $res[0]->PROP_ADD,
		  'botanical_name' => $res[0]->New_BotNam,
		  'common_name' => $res[0]->New_ComNam,
		  'comments' => $res[0]->Comments,
		  'status' => $res[0]->status,
		  'status_changed' => $res[0]->status_changed,
		  'status_changed_user' => $res[0]->status_changed_user,
	  );
	  
	  return json_encode($return);
  }
  
  function getNearestAddress(Request $request) {
	  \Debugbar::disable();
	  $vals = explode("|", $request->req);
	  
	  $url = 'https://maps.googleapis.com/maps/api/geocode/json?latlng=' . $vals[0] . ',' . $vals[1] . '&location_type=ROOFTOP&result_type=street_address&key=AIzaSyDGZpFGOzgDZSD5WAbOB65MFNyevBOZW3E';
	  $ch = curl_init($url);
	  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	  $res = json_decode(curl_exec($ch));
	  
	  curl_close($ch);
	  if($res->results){
		  $addr_components = $res->results[0]->address_components;
		  $addr_long = $res->results[0]->formatted_address;
	  
		  return($addr_long);
	  } else {
		  return("Could not find nearby address");
	  }
  }
  
  function getSearchData() {
    ini_set('precision', 17); //set precision to prevent casting losing number
    
    $trees = ProposedTree::get();
    
    $reps = array();
    $reps['type'] = 'FeatureCollection';
    $reps['features'] = array();

    $incI = 0;
    foreach($trees as $item) {
      $reps['features'][$incI]['type'] = "Feature";
      $reps['features'][$incI]['geometry'] = array(
        'type' => "Point",
        'coordinates' => [floatval($item->LONGITUDE), floatval($item->LATITUDE)]
      );
      $reps['features'][$incI]['properties'] = array(
	    'id' => $item->ID1,
	    'asset_id' => $item->ASSET_ID,
        'street_planted' => $item->ST_PLANTED,
        'property_address' => $item->PROP_ADD,
        'house_number' => $item->HOUSE_NUMB,
        'street_name' => $item->STREET_NAM,
        'suburb' => $item->SUBURB,
        'postcode' => $item->POSTCODE,
        'project' => $item->PROJECT,
        'verge_width' => $item->VERGE_WIDTH,
        'collector' => $item->COLLECTOR,
        'previous_botanical_name' => $item->EX_BotNam,
        'previous_common_name' => $item->Ex_ComNam,
        'action' => $item->Action,
        'current_botanical_name' => $item->New_BotNam,
        'current_common_name' => $item->New_ComNam,
        'comments' => $item->Comments,
        'type' => $item->Type,
        'status' => $item->status,
        'status_changed' => $item->status_changed,
      );
      $incI++;
    }

    return response()->json($reps);
  }
  
  function getTreeComments(Request $request){
	  $comments = TreeComments::where('tree_id', $request->req)->orderBy('msg_id', 'asc')->get();
	  return response()->json($comments);
  }

  function getTreeCommentsRendered(Request $request){
    $return = "";
    $comments = TreeComments::where('tree_id', $request->req)->orderBy('msg_id', 'asc')->get();
    foreach ($comments as $key => $comment) {

      $username = json_decode($this->_userFromID($comment->user_id))[0]->name;
      $currTime = time();
      $time = strtotime($comment->timestamp);
      if($time > strtotime('-1 day', $currTime)){
        $timestamp = " at " . date('h:i', $time);
      } else if ($time > strtotime('-1 week', $currTime)){
        $timestamp = " on " . date('D', $time);
      } else {
        $timestamp = " on " . date('j/m', $time);
      }

      if($comment->user_id == \Illuminate\Support\Facades\Auth::id()){
        $return .= "<div class='my_comment'>";
      } else {
        $return .= "<div class='comment'>";
      }
      $return .= "<p class='comment_message'>";
      $return .= $comment->message;
      $return .= "</p><small class='comment_footer'>- ";
      $return .= $username . $timestamp . "</small>";
      $return .= "</div>";
    }
	  return $return;
  }
  
  function addTreeComment(Request $request){
	  $vals = explode("|", $request->req);
	  
	  $datetime = new \DateTime("now");
	  
	  $userid = \Illuminate\Support\Facades\Auth::id();
	  
	  if(!$userid)
	  	$userid = 1;
	  	
	  TreeComments::insert([
		  'tree_id' => $vals[0],
		  'user_id' => $userid,
		  'timestamp' => $datetime->format("Y-m-d\TH:i:s"),
		  'message' => $vals[1],
	  ]);
  }
  
  function _userFromID($userid) {
	  return \Illuminate\Support\Facades\DB::table('TP_users')->select('name')->where('id', $userid)->get();
  }

  function userFromID(Request $request) {
	  $vals = explode("|", $request->req);
	  
	  return \Illuminate\Support\Facades\DB::table('TP_users')->select('name')->where('id', $vals[0])->get();
  }
  
  function uploadImage(Request $request) {
    $image = $request->object;
    AuditPhotos::insert([
      'audit_type' => $request->audit_type,
      'tree_id' => $request->tree_id,
      'object' => $image,
    ]);

    return response()->json(['success' => true]);
  }

  function getPlantingAuditPhotoThumbs(Request $request) {
    $data = AuditPhotos::where('tree_id', $request->req)->get();
    $resp = Array();
    foreach ($data as $key => $value) {
      $image = base64_decode($value->object);
      // Resize image
      $im = \imagecreatefromstring($image);
      //FIRST: Get image width, height, and select smallest

      //SECOND: Get image X and Y 
      
      //imagecopyresampled($resampled_im, $im, 0, 0, $src_x, $src_y, 128, 128, $src_w, $src_h);
      // base64_encode image
      $thisphoto = Array();
      $thisphoto['image'] = $image;
      $thisphoto['id'] = $value->file_id;
      array_push($resp, $thisphoto);
    }
    return response()->json($resp);
  }
  
  function index(Request $request) {
    ini_set('precision', 17); //set precision to prevent casting losing number
    
    $trees = $this->GetAllProposedTrees($request);
    $reps = array();
    $reps['type'] = 'FeatureCollection';
    $reps['features'] = array();

    $incI = 0;
    foreach($trees as $item) {
      $reps['features'][$incI]['type'] = "Feature";
      $reps['features'][$incI]['geometry'] = array(
        'type' => "Point",
        'coordinates' => [floatval($item->LONGITUDE), floatval($item->LATITUDE)]
      );
      $reps['features'][$incI]['properties'] = array(
	    'id' => $item->ID1,
	    'asset_id' => $item->ASSET_ID,
        'street_planted' => $item->ST_PLANTED,
        'property_address' => $item->PROP_ADD,
        'house_number' => $item->HOUSE_NUMB,
        'street_name' => $item->STREET_NAM,
        'suburb' => $item->SUBURB,
        'postcode' => $item->POSTCODE,
        'project' => $item->PROJECT,
        'verge_width' => $item->VERGE_WIDTH,
        'collector' => $item->COLLECTOR,
        'previous_botanical_name' => $item->EX_BotNam,
        'previous_common_name' => $item->Ex_ComNam,
        'action' => $item->Action,
        'current_botanical_name' => $item->New_BotNam,
        'current_common_name' => $item->New_ComNam,
        'comments' => $item->Comments,
        'type' => $item->Type,
        'status' => $item->status,
        'status_changed' => $item->status_changed,
      );
      $incI++;
    }

    return response()->json($reps);
  }
}
