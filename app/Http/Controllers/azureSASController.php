<?php

namespace App\Http\Controllers;
/**
 * From https://gist.github.com/jcorioland/731d7ab2aac9b505120a1a80a0188692
 * 
 * Generates a shared access signature for Microsoft Azure storage
 * cf. https://docs.microsoft.com/en-us/rest/api/storageservices/fileservices/constructing-an-account-sas 
 *
 * @param (accountName) The name of the Microsoft Azure storage account
 * @param (storageKey) The access key of the Microsoft Azure storage account
 * @param (signedPermissions) Required. Specifies the signed permissions for the account SAS
 * @param (signedService) Required. Specifies the signed services accessible with the account SAS
 * @param (signedResourceType) Required. Specifies the signed resource types that are accessible with the account SAS.
 * @param (signedStart) Optional. The time at which the SAS becomes valid, in an ISO 8601 format. 
 * @param (signedExpiry) Required. The time at which the shared access signature becomes invalid, in an ISO 8601 format.
 * @param (signedIP) Optional. Specifies an IP address or a range of IP addresses from which to accept requests.
 * @param (signedProtocol) Optional. Specifies the protocol permitted for a request made with the account SAS.
 * @param (signedVersion) Required. Specifies the signed storage service version to use to authenticate requests made with this account SAS
 * @return The shared access signature encoded as base64
 */

class azureSASController extends Controller {
    function generateSharedAccessSignature($accountName, 
        $storageKey, 
        $signedPermissions, 
        $signedService, 
        $signedResourceType, 
        $signedStart, 
        $signedExpiry, 
        $signedIP, 
        $signedProtocol, 
        $signedVersion){

        if(empty($accountName)){
            trigger_error("The account name is required.");
            return;
        }

        if(empty($storageKey)){
            trigger_error("The account key is required.");
            return;
        }

        if(empty($signedPermissions)){
            trigger_error("The permissions are required.");
            return;
        }

        if(empty($signedService)){
            trigger_error("The services are required.");
            return;
        }

        if(empty($signedResourceType)){
            trigger_error("The resource types are required.");
            return;
        }

        if(empty($signedExpiry)){
            trigger_error("The expiration time is required.");
            return;
        }

        if(empty($signedVersion)){
            trigger_error("The service version is required.");
            return;
        }


        // generate the string to sign
        $_toSign = urldecode($accountName) . "\n" . 
                urldecode($signedPermissions) . "\n" . 
                urldecode($signedService) . "\n" . 
                urldecode($signedResourceType) . "\n" . 
                urldecode($signedStart) . "\n" .
                urldecode($signedExpiry) . "\n" .
                urldecode($signedIP) . "\n" .
                urldecode($signedProtocol) . "\n" .
                urldecode($signedVersion) . "\n";

        // sign the string using hmac sha256 and get a base64 encoded version_compare
        $_signature = base64_encode(hash_hmac("sha256", utf8_encode($_toSign), base64_decode($storageKey), true));

        return $_signature;
    }

    function getBlobUrlWithSharedAccessSignature($blobUri,
        $signedVersion,
        $signedService,
        $signedResourceType,
        $signedPermissions,
        $signedStart,
        $signedExpiry,
        $signedIP,
        $signedProtocol,
        $signature) {

        
        /* Create the signed query part */
        $_urlParts = array();
        $_urlParts[] = "sv=" . $signedVersion;
        $_urlParts[] = "ss=" . $signedService;
        $_urlParts[] = "srt=" . $signedResourceType;
        $_urlParts[] = "sp=" . $signedPermissions;
        $_urlParts[] = "st=" . $signedStart;
        $_urlParts[] = "se=" . $signedExpiry;
        $_urlParts[] = "spr=" . $signedProtocol;
        $_urlParts[] = "sig=" . urlencode($signature);

        $_blobUrlWithSAS = implode("&", $_urlParts);

        return $_blobUrlWithSAS;
    }

    function index(){
        $_storageKey = "j+KLWklDA5NP8CLcyXCAuQ4y44YjiVboXAvC/FvEjPRBUk5fXBOkGxvNALK/iV43wvKee/WlWSyPOehud+bO3A==";
        $_accountName = "treeinspectphotosdev";
        $_signedPermissions = "rw";
        $_signedService = "b";
        $_signedResourceType = "sco";
        $_signedStart = Date("Y-m-d\TH:i:s\Z", strtotime("yesterday"));// "2017-02-23T00:00:00Z";
        $_signedExpiry = Date("Y-m-d\TH:i:s\Z", strtotime("next year"));
        $_signedIP = "";
        $_signedProtocol = "https";
        $_signedVersion = "2017-04-17";

        // generate the signature
        $_signature = $this->generateSharedAccessSignature($_accountName, 
            $_storageKey, 
            $_signedPermissions, 
            $_signedService, 
            $_signedResourceType, 
            $_signedStart, 
            $_signedExpiry, 
            $_signedIP, 
            $_signedProtocol, 
            $_signedVersion);

        //echo "SIGNATURE = " . $_signature;
        //echo "<br /><br />";

        $_blobUrl = $this->getBlobUrlWithSharedAccessSignature("",
            $_signedVersion,
            $_signedService,
            $_signedResourceType,
            $_signedPermissions,
            $_signedStart,
            $_signedExpiry,
            $_signedIP,
            $_signedProtocol,
            $_signature);

        //echo "BLOB URL = " . $_blobUrl;
        //echo "<br /><br />";
        return $_blobUrl;
    }
}