<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AuditData;

class AuditController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json("");
    }

    public function getLatestAudit(Request $request)
    {
        $tree_id = $request->tree_id;

        return response()->json(AuditData::where('tree_id', $tree_id)->orderBy('audit_time', 'desc')->first()->get());
    }

    public function createAudit(Request $request)
    {
        // Get the data from the URL string
        $audit_data = array();
        parse_str($request->audit_data, $audit_data);
        
        // Verify data
        // Do we have a tree and an inspector?
        if(!$audit_data['tree_id'] || !$audit_data['operator_id'])
            return;

        // Checkboxes only return a value if they're checked, so
        // we need to fill in the blanks if we're missing values.
        if(!isset($audit_data['is_healthy']))
            $audit_data['is_healthy'] = false;

        if(!isset($audit_data['is_strong']))
            $audit_data['is_strong'] = false;

        if(!isset($audit_data['hole_size']))
            $audit_data['hole_size'] = false;

        if(!isset($audit_data['gypsum_applied']))
            $audit_data['gypsum_applied'] = false;

        if(!isset($audit_data['root_ball_pruned']))
            $audit_data['root_ball_pruned'] = false;

        if(!isset($audit_data['hole_backfilled']))
            $audit_data['hole_backfilled'] = false;

        if(!isset($audit_data['waterwell_placed']))
            $audit_data['waterwell_placed'] = false;

        if(!isset($audit_data['is_mulched']))
            $audit_data['is_mulched'] = false;

        if(!isset($audit_data['stakes_place']))
            $audit_data['stakes_place'] = false;

        if(!isset($audit_data['tree_watered']))
            $audit_data['tree_watered'] = false;

        // Insert a new record
        $audit = new AuditData;

        $audit->tree_id = $audit_data['tree_id'];
        $audit->operator_id = $audit_data['operator_id'];
        $audit->is_healthy = $audit_data['is_healthy'];
        $audit->is_strong = $audit_data['is_strong'];
        $audit->hole_size = $audit_data['hole_size'];
        $audit->gypsum_applied = $audit_data['gypsum_applied'];
        $audit->root_ball_pruned = $audit_data['root_ball_pruned'];
        $audit->hole_backfilled = $audit_data['hole_backfilled'];
        $audit->waterwell_placed = $audit_data['waterwell_placed'];
        $audit->is_mulched = $audit_data['is_mulched'];
        $audit->stakes_place = $audit_data['stakes_place'];
        $audit->tree_watered = $audit_data['tree_watered'];

        $audit->save();

        return response()->json($audit_data);
    }
}
