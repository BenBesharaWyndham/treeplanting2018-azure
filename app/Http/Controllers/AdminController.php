<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TI_ProposedPlantings;

ini_set('max_execution_time', 600);

class AdminController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
    $this->middleware('auth');
  }

  /**
   * Show the application dashboard.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    return view('dashboard.admin.home');
  }

  public function upload()
  {
    return view('dashboard.admin.upload');
  }

  public function doUpload(Request $request)
  {
//  get file
    $upload = $request->file('file');
    $filePath = $upload->getRealPath();

//  open and read
    $file = fopen($filePath, 'r');
    $header = fgetcsv($file);

//  global var
    $escapedHeader = [];

//  validate Header
    foreach ($header as $key => $item) {
      $lheader = strtolower($item);
      $escapedItem = preg_replace('/[^a-z]/', '', $lheader);
      array_push($escapedHeader, $escapedItem);
    }

//  looping through columns
    while ($columns = fgetcsv($file)) {
      if ($columns[0] == "") {
        continue;
      }

//    validate data
      foreach ($columns as $key => $value) {
//      replace illegal charactor to space
        preg_replace('/[^A-Za-z0-9,.#\/\'\s\-$]/', '', $value);
      }

      $data = array_combine($escapedHeader, $columns);

      $plantingID = $data['plantingid'];
      $nearAddress = $data['nearaddress'];
      $notifyAddress = $data['notifyaddress'];
      $latitude = $data['latitude'];
      $longitude = $data['longitude'];
      $locality = $data['locality'];
      $treeCommon = $data['treecommon'];
      $treeBotanical = $data['treebotanical'];
      $treeOrigin = $data['treeorigin'];
      $status = $data['status'];

      if ($plantingID == 0 && $status == 'insert') {

        $tree = new TI_ProposedPlantings;
        $tree->near_address = $nearAddress;
        $tree->notify_address = $notifyAddress;
        $tree->latitude = $latitude;
        $tree->longitude = $longitude;
        $tree->locality = $locality;
        $tree->tree_common = $treeCommon;
        $tree->tree_botanical = $treeBotanical;
        $tree->tree_origin = $treeOrigin;
        $tree->archived = false;
        $tree->display = true;
        $tree->status = 'insert';
        $tree->save();

      }

      if ($status == 'update' && $plantingID != 0) {

        $tree = TI_ProposedPlantings::find($plantingID);

        if (!empty($tree)) {
          $tree->near_address = $nearAddress;
          $tree->notify_address = $notifyAddress;
          $tree->latitude = $latitude;
          $tree->longitude = $longitude;
          $tree->locality = $locality;
          $tree->tree_common = $treeCommon;
          $tree->tree_botanical = $treeBotanical;
          $tree->tree_origin = $treeOrigin;
          $tree->archived = false;
          $tree->display = true;
          $tree->status = 'update';
          $tree->save();
        } else {
//          throw exception
        }

      }

      if ($status == 'delete' && $plantingID != 0) {

        $tree = TI_ProposedPlantings::find($plantingID);

        if (!empty($tree)) {
          $tree->archived = true;
          $tree->display = false;
          $tree->status = 'delete';
          $tree->save();
        } else {
//          throw exception
        }

      }
    }

    return response()->json(true);
  }

  public function findID(Request $request)
  {
    $tree = TI_ProposedPlantings::find($request->plantingID);
    return response()->json($tree);
  }

  public function findNearAdd(Request $request)
  {
    $tree = TI_ProposedPlantings::where('near_address', 'like', '%'.$request->nearAdd.'%')->get();
    return response()->json($tree);
  }

  public function findDateTime(Request $request)
  {
    $tree = TI_ProposedPlantings::whereBetween('date_created', [$request->fromDate, $request->toDate])->get();
    return response()->json($tree);
  }

  public function delete(Request $request)
  {
    TI_ProposedPlantings::whereIn('planting_id', $request->ids)->update(['archived' => true, 'display' => false, 'status' => 'delete']);
  }
}