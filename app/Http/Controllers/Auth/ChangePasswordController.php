<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class ChangePasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/home';
    
    public function index(\Illuminate\Http\Request $request)
    {
	    if($_POST){
			$email = $request->email;
			$password = $request->password;
			$password_conf = $request->password_confirmation;
			if($password == $password_conf){
				//reset password
				$user = \Illuminate\Support\Facades\Auth::user();
				$user->touch();
				DB::table('TP_users')->where("email", $email)->update(["password" => Hash::make($password)]);
				return redirect('/');
			} else {
				return view('auth.passwords.change')->with(
					['email' => $email, 'error' => "Passwords do not match"]
				);
			}
	    }
		
		$email = \Illuminate\Support\Facades\Auth::user()->email;
		
	    return view('auth.passwords.change')->with(
            ['email' => $email]
        );
    }
	
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }
}
