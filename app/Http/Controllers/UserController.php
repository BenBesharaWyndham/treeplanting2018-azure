<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\UserRoles;

class UserController extends Controller
{
    function getListOfUsers(){
        $users = User::all();
        $i = 0;
        foreach ($users as $key => $user) {
            $users[$i]->role_name = $user->role()->get()[0]->role_name;
            $users[$i]->role_id = $user->role()->get()[0]->id;
            $i++;
        }
        return response()->json($users);
    }

    function getRoles(){
        return response()->json(UserRoles::all());
    }
    
    function saveRoles(Request $request){
        $users = array();
        parse_str($request->req, $users);

        // This is kind of dumb but neccesary because
        // of the way PHP's parsing works
        $users = $users['users'];
        foreach($users as $userdata){
            $user = User::find($userdata['id']);
            $user->email = $userdata['email'];
            $user->role()->detach($user->role()->get()[0]->id);
            $user->role()->attach($userdata['role']);
            $user->save();
        }
        return response()->json(['success' => 'true']);
    }
}