<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\MapController;

class AuditData extends Model
{
	protected $primary_key = 'id';
	protected $table = 'TP_audit_data';
	public $timestamps = false;
	
	public function getKeyName(){
    	return "id";
	}
}