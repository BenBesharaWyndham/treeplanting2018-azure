<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class UserRoles extends Model {
    protected $primary_key = 'id';
    protected $table = 'TP_user_roles';

    public function users(){
        return $this->belongsToMany('App\User');
    }
}
