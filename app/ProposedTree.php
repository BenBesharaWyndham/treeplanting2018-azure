<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\MapController;

class ProposedTree extends Model
{
	public $timestamps = false;
    protected $primaryKey = 'ID1';
    protected $table = 'TreePlantingSeason2018';
    protected $hidden = ['Geometry', 'Geometry_SPA', 'EASTING', 'NORTHING', ];
    // protected $casts = [
    //     'latitude' => 'double',
    //     'longitude' => 'double'
    // ];

    protected function getDateFormat()
    {
        return 'Y-m-d H:i:s.u0';
    }
    
    public function comments()
    {
        return $this->hasMany('App\TreeComments', 'tree_id', 'ID1');
    }
    
}

/*class TreePlantingStatus extends Model
{
	protected $primary_key = 'Tree_ID';
	protected $table = 'TP_planted';
	
	protected $fillable = ['Tree_ID', 'is_planted'];
	
	public function getKeyName(){
    	return "Tree_ID";
	}
}*/