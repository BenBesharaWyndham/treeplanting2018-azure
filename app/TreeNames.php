<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\MapController;

class TreeNames extends Model
{
	protected $primary_key = 'tree_type_id';
	protected $table = 'TI_Tree_Types';
	protected $hidden = ['tree_origin', 'modified_date_obj', 'date_updated', 'archived'];
	
	public function getKeyName(){
    	return "tree_type_id";
	}
}