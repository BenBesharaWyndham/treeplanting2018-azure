<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'MapController@showMap')->middleware('checkpassword');//->middleware('auth');

Route::get('/List', 'ListController@showList');//->middleware('auth');
Route::get('/getListOfTrees', 'ListController@GetListOfTrees');//->middleware('auth');

Route::get('/app', 'MapController@showMap')->middleware('auth');

Route::get('/getGeoJson/{req}', 'MapController@index');//->middleware('auth');

Route::get('/updateMarkerPosition/{req}', 'MapController@updateMarkerPosition');//->middleware('auth');

Route::get('/updateMarkerMetadata/{req}', 'MapController@updateMarkerMetadata');//->middleware('auth');

Route::get('/getMarkerMetadata/{req}', 'MapController@getMarkerMetadata');//->middleware('auth');

Route::get('/getNextTreeID', 'MapController@getNextTreeID');//->middleware('auth');

Route::get('/getTreeNameArray', 'MapController@getTreeNameArray');//->middleware('auth');

Route::get('/getFuzzyTreeNameSearch/{req}', 'MapController@getFuzzyTreeNameSearch');

Route::get('/getListOfUsers', 'ListController@getListOfUsers');

Route::get('/getListOfTreesInDateRange/{req}', 'ListController@getListOfTreesInDateRange');

Route::get('/getTreeNameIDFromName/{req}', 'MapController@getTreeNameIDFromName');//->middleware('auth');

Route::get('/createNewMarker/{req}', 'MapController@createNewMarker');//->middleware('auth');

Route::get('/updateTreeStatus/{req}', 'MapController@updateTreeStatus');//->middleware('auth');

Route::get('/getNearestAddress/{req}', 'MapController@getNearestAddress');//->middleware('auth');

Route::get('/getTreeComments/{req}', 'MapController@getTreeComments');

Route::get('/getTreeCommentsRendered/{req}', 'MapController@getTreeCommentsRendered');

Route::get('/addTreeComment/{req}', 'MapController@addTreeComment');

Route::get('/getSearchData', 'MapController@getSearchData');

Route::get('/userFromID/{req}', 'MapController@userFromID');

Route::get('/generateSAS', 'azureSASController@index');

Route::post('/uploadPlantingAuditPhoto', 'MapController@uploadImage');

Route::get('/getPlantingAuditPhotoThumbs/{req}', 'MapController@getPlantingAuditPhotoThumbs');

Route::get('/temp', function() {
  return view('dashboard.showMap');
});

Route::get('/api/getListOfUsers', 'UserController@getListOfUsers');

Route::get('/api/getRoles', 'UserController@getRoles');

Route::get('/api/saveRoles/{req}', 'UserController@saveRoles');

Route::get('/api/getLatestAudit/{tree_id}', 'AuditController@getLatestAudit');

Route::get('/api/createAudit/{audit_data}', 'AuditController@createAudit');

/* user management */
Auth::routes();

Route::get('/password/change', 'Auth\ChangePasswordController@index');

Route::post('/password/change', 'Auth\ChangePasswordController@index');

Route::get('/home', 'MapController@showMap');

Route::group(['prefix' => 'admin'], function() {
  Route::get('home', 'AdminController@index')->name('admin.home');

  Route::get('upload', 'AdminController@upload')->name('admin.upload');
  Route::post('doUpload', 'AdminController@doUpload')->name('admin.doUpload');

  Route::post('findID', 'AdminController@findID')->name('admin.findID');
  Route::post('findNearAdd', 'AdminController@findNearAdd')->name('admin.findNearAdd');
  Route::post('findDateTime', 'AdminController@findDateTime')->name('admin.findDateTime');

  Route::post('delete', 'AdminController@delete')->name('admin.delete');
});