<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

////Route::group(['prefix' => 'v1', 'middleware' => 'api'], function(){
////  Route::resource('trees/{req}', 'MapController');
////});
//
//Route::get('/fetch_all_proposed', 'MapController@GetAllProposedTrees');
//
//Route::get('/welcome', function() {
//  return view('welcome');
//});
//
//Route::get('/trees/{req}', 'MapController@GetMarkers');