<!DOCTYPE html>
<html>
<head>
    <title>Data Layer: Styling</title>
    <meta name="viewport" content="initial-scale=1.0">
    <meta charset="utf-8">
    <style>
        /* Always set the map height explicitly to define the size of the div
         * element that contains the map. */
        #map {
            height: 100%;
        }
        /* Optional: Makes the sample page fill the window. */
        html, body {
            height: 100%;
            margin: 0;
            padding: 0;
        }
    </style>
</head>
<body>
<div id="map"></div>
<script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.2.1.min.js"></script>
<script>
function initMap() {
    var map;
    var markers;
    var myLatlng = new google.maps.LatLng(-37.88, 144.70);
    var infowindow = new google.maps.InfoWindow({});

    map = new google.maps.Map(document.getElementById('map'), {
        zoom: 15,
        center: myLatlng,
        mapTypeId: 'roadmap'
    });

    var mcOptions = {gridSize: 50, maxZoom: 15, imagePath: 'https://cdn.rawgit.com/googlemaps/v3-utility-library/master/markerclustererplus/images/m'};
    var mc = new MarkerClusterer(map, [], mcOptions);

    google.maps.event.addListener(map, 'click', function() {
        infowidow.close();
    });

    map.addListener('idle', function() {

        var b = map.getBounds();
        var NE = b.getNorthEast();
        var SW = b.getSouthWest();

        var boundsNElat = NE.lat();
        var boundsSWlat = SW.lat();
        var boundsNElong = SW.lng();
        var boundsSWlong = NE.lng();

        var req = [boundsNElat, boundsSWlat, boundsNElong, boundsSWlong];

//        console.log('http://wynhdam.dev3/api/trees/'+req);

        if(markers != null && markers.length != 0) {
            for(i=0; i<markers.length; i++) {
              map.data.remove(markers[i]);
            }
        }

        map.data.loadGeoJson('http://wynhdam.dev3/api/trees/'+req, {}, function(features) {
            for(i=0; i<features.length; i++) {
                console.log(features[i].getGeometry());
            }
        });

//        google.maps.event.addListener(map.data, 'addfeature', function (e) {
//          if (e.feature.getGeometry().getType() === 'Point') {
//            var marker = new google.maps.Marker({
//              position: e.feature.getGeometry().get(),
//              title: e.feature.getProperty('name'),
//              map: map
//            });
//          }
//          mc.addMarker(marker);
//        });

        map.data.addListener('click', function(event) {
            infowindow.setContent('<div id="content">'+'<div id="bodyContent">'+event.feature.getProperty('near_address')+'</div>'+'</div>');
            infowindow.setOptions({
                pixelOffset: new google.maps.Size(0, -36)
            });
            infowindow.setPosition(event.feature.getGeometry().get());
            infowindow.open(map);
            google.maps.event.addListenerOnce(infowindow, 'domready', function() {
                infowindow.close();
                infowindow.open(map);
            });
        });
    });
}
</script>
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDGZpFGOzgDZSD5WAbOB65MFNyevBOZW3E&callback=initMap">
</script>
<script type="text/javascript" src="/js/markerclusterer.js"></script>
</body>
</html>