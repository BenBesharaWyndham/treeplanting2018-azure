<!DOCTYPE html>
<html>
<head>
  <title>GeoJSON tutorial - Leaflet</title>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="shortcut icon" type="image/x-icon" href="docs/images/favicon.ico" />
  <link rel="stylesheet" href="https://unpkg.com/leaflet@1.0.3/dist/leaflet.css" integrity="sha512-07I2e+7D8p6he1SIM+1twR5TIrhUQn9+I6yjqD53JQjFiMf8EtC93ty0/5vJTZGF8aAocvHYNEDJajGdNx1IsQ==" crossorigin=""/>
  <link rel="stylesheet" href="{{ asset('/css/MarkerCluster.css') }}" />
  <link rel="stylesheet" href="{{ asset('/css/MarkerCluster.Default.css') }}" />

  <style>
    #map {
      height: 100%;
    }
    html, body {
      height: 100%;
      margin: 0;
      padding: 0;
    }
  </style>

</head>
<body>
<div id='map'></div>
<script src="https://unpkg.com/leaflet@1.0.3/dist/leaflet.js" integrity="sha512-A7vV8IFfih/D732iSSKi20u/ooOfj/AGehOKq0f4vLT1Zr2Y+RX7C+w8A1gaSasGtRUZpF/NZgzSAu4/Gc41Lg==" crossorigin=""></script>
<script
  src="https://code.jquery.com/jquery-1.12.4.min.js"
  integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ="
  crossorigin="anonymous"></script>
<script src="{{ asset('/js/leaflet.markercluster-src.js') }}"></script>
<script>
//  function initGeolocation()
//  {
//    if (navigator.geolocation) {
//      navigator.geolocation.getCurrentPosition(successFunction, errorFunction);
//    } else {
//      alert('It seems like Geolocation, which is required for this page, is not enabled in your browser. Please use a browser which supports it.');
//    }
//  }
//
//  function successFunction(position) {
//    var lat = position.coords.latitude;
//    var long = position.coords.longitude;
//    console.log('Your latitude is :'+lat+' and longitude is '+long);
//    loadGeojson(lat, long);
//  }
//
//  function errorFunction()
//  {
//    // Could not obtain location
//  }

  function onEachFeature(feature, layer) {
    var popupContent = "";

    if (feature.properties && feature.properties.near_address) {
      popupContent += feature.properties.near_address;
    }

    layer.bindPopup(popupContent);
  }

  function loadGeojson(lat, long)
  {
    var req = -37.8782831086417+','+-37.891023983938+','+144.680405585268+','+144.700885230972;
    console.log(req);
    var map = L.map('map').setView([lat, long], 17);
    var markers = (function() {
      var json = null;
      $.ajax({
        'async': false,
        'global': false,
        'url': "/getGeoJson/"+req,
        'dataType': "json",
        'success': function (data) {
          json = data;
        }
      });
      return json;
    })();

//    L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
//      maxZoom: 18,
//      attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, ' +
//      '<a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
//      'Imagery © <a href="http://mapbox.com">Mapbox</a>',
//      id: 'mapbox.light'
//    }).addTo(map);


    var geojson = L.geoJSON([markers], {

      style: function (feature) {
        return feature.properties && feature.properties.style;
      },

      onEachFeature: onEachFeature,

      pointToLayer: function (feature, latlng) {
        return L.marker(latlng);
      }
    });

    var markers = L.markerClusterGroup();
    markers.addLayer(geojson);

    map.addLayer(markers);
  }

  $(document).ready(function() {
//    initGeolocation();
    loadGeojson(-37.877699, 144.681702);
  });
</script>
</body>
</html>