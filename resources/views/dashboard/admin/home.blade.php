@extends('layouts.app')

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="panel panel-default">
          <div class="panel-heading"><h3>Admin Dashboard</h3></div>

          <div class="panel-body">
            <div class="header"><h4><i class="fa fa-user" aria-hidden="true"></i> User Management</h4></div>
            <hr>
            <a href="" class="btn btn-primary btn-lg"><i class="fa fa-user" aria-hidden="true"></i> Create</a>
            <a href="" class="btn btn-success btn-lg"><i class="fa fa-user" aria-hidden="true"></i> Update</a>
            <a href="" class="btn btn-danger btn-lg"><i class="fa fa-user" aria-hidden="true"></i> Delete</a>

            <div class="header"><h4><i class="fa fa-tree" aria-hidden="true"></i> Tree Management</h4></div>
            <hr>
            <a href="" class="btn btn-primary btn-lg"><i class="fa fa-tree" aria-hidden="true"></i> Create</a>
            <a href="/admin/upload" class="btn btn-default btn-lg"><i class="fa fa-tree" aria-hidden="true"></i> Upload</a>
            <a href="" class="btn btn-success btn-lg"><i class="fa fa-tree" aria-hidden="true"></i> Update</a>
            <a href="" class="btn btn-danger btn-lg"><i class="fa fa-tree" aria-hidden="true"></i> Delete</a>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
