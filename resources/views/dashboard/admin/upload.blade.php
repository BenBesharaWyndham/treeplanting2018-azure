@extends('layouts.app')

@section('style')
  <link rel="stylesheet" href="{{ asset('css/bootstrap-datepicker.min.css') }}">
  <link rel="stylesheet" href="{{ asset('css/jquery.dataTables.min.css') }}">
  <link rel="stylesheet" href="{{ asset('css/buttons.dataTables.min.css') }}">
@endsection

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="panel panel-default">
          <div id="loading" style="display: none;"><img src="{{ asset('images/loaderIcon.gif') }}" /></div>
          <div class="alert alert-success" id="success" style="display: none;">
            <strong>Success!</strong> file has been uploaded.
          </div>
          <div class="alert alert-danger" id="error" style="display: none;">
            <strong>Alert!</strong> please upload CSV file.
          </div>
          <div class="panel-heading"><h3>Admin Dashboard</h3></div>

          <div class="panel-body">
            <div class="header"><h4><i class="fa fa-tree" aria-hidden="true"></i> Upload Trees CSV files</h4></div>
            <hr>
            <form action="{{ url('/admin/doUpload') }}" method="post" enctype="multipart/form-data" id="myForm" name="myForm">
              {{ csrf_field() }}
              <div class="form-group">
                <label for="upload_file">Upload files</label>
                <input type="file" name="file" id="file" class="form-control"/>
              </div>
              <input type="submit" class="btn btn-primary" name="Upload" value="Upload&Import">
            </form>
          </div>

          <div class="panel-body">
            <div class="header"><h4><i class="fa fa-tree" aria-hidden="true"></i> Filter Tree Result</h4></div>
            <hr>

            <div class="col-md-6">
              <form action="{{ url('admin/findID') }}" method="post" id="searchIDForm" name="searchIDForm">
                {{ csrf_field() }}
                <div class="form-group">
                  <label for="plantingID">Filter By Planting ID</label>
                  <div class="input-group">
                    <input type="text" name="plantingID" id="plantingID" class="form-control" placeholder="Enter planting ID here.">
                    <div class="input-group-btn">
                      <input type="submit" class="btn btn-success" name="searchID" value="Find">
                    </div>
                  </div>
                </div>
              </form>
            </div>

            <div class="col-md-6">
              <form action="{{ url('admin/findNearAdd') }}" method="post" id="searchAddrForm" name="searchAddrForm">
                {{ csrf_field() }}
                <div class="form-group">
                  <label for="plantingID">Filter By Near Address</label>
                  <div class="input-group">
                    <input type="text" name="nearAdd" id="nearAdd" class="form-control" placeholder="Enter address here.">
                    <div class="input-group-btn">
                      <input type="submit" class="btn btn-success" name="searchNearAdd" value="Find">
                    </div>
                  </div>
                </div>
              </form>
            </div>

            <div>
              <form action="{{ url('admin/findDateTime') }}" method="post" id="searchDateTime" name="searchDateTime">
                {{ csrf_field() }}
                <label for="fromDate" style="margin-left: 15px;">Filter By Range Of Date</label>
                <div class="form-group">
                  <div class="col-md-5"><input type="text" class="form-control" id="fromDate" name="fromDate"></div>
                  <div class="col-md-5"><input type="text" class="form-control" id="toDate" name="toDate"></div>
                  <div class="col-md-2">
                    <input type="submit" class="btn btn-success" name="searchDate" value="Find">
                  </div>
                </div>
              </form>
            </div>
          </div>

          <div class="panel-body">
            <div class="header"><h4><i class="fa fa-tree" aria-hidden="true"></i> Result</h4></div>
            <hr>
            <div id="result">

            </div>
          </div>

        </div>
      </div>
    </div>
  </div>
@endsection

@section('script')
  <script src="{{ asset('js/bootstrap-datepicker.min.js') }}"></script>
  <script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('js/dataTables.buttons.min.js') }}"></script>
  <script src="{{ asset('js/buttons.flash.min.js') }}"></script>
  <script src="{{ asset('js/jszip.min.js') }}"></script>
  <script src="{{ asset('js/pdfmake.min.js') }}"></script>
  <script src="{{ asset('js/vfs_fonts.js') }}"></script>
  <script src="{{ asset('js/buttons.html5.min.js') }}"></script>
  <script src="{{ asset('js/buttons.print.min.js') }}"></script>
  <script src="{{ asset('js/main.js') }}"></script>
@endsection