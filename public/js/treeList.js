$(document).ready(function() {
    var dataTable = $('#treeList').DataTable({
        "ajax": 'https://digital.wyndham.vic.gov.au/treeplanting/getListOfTrees',
        "columns": [
            { 
	            "data": "ID",
	            "name": "id"
	        },
            { 
	            "data": "Address",
	            "name": "address"
	        },
            {
	            "data": "Botanical Name",
	            "name": "botanical_name"
	        },
            {
	            "data": "Common Name",
	            "name" : "common_name"
	        },
            {
	            "data": "Status",
	            "name": "status"
	        },
            { 
	            "data": "Date Updated",
	            "name": "date_updated"
	        },
            { 
	            "data": "User",
	            "name": "user"
	        },
            {
	            "data": "Comments",
	            "name": "comments"
	        }
        ],
        "dom": 'lBfrtip',
        "buttons": [
	        'excelHtml5',
	        'pdfHtml5'
        ],
        "initComplete": function(settings, json){
	        
        }
    });
    
    //Tree status filter
    var filters = document.createElement("div");
    $("#treeList").before(filters);
    if(window.screen.width < 700){
	    $(filters).css("display", "inline-block");
    }
    $(filters).append('<select id="filter_status" />');
    $("#filter_status").append('<option value="">Tree Status</option>');
    var status = [
        'unchecked',
        'unsuitable',
        'ready',
        'planted'
    ];
    
    $.each(status, function(k, v){
        $("#filter_status").append('<option value="' + v + '">' + v + '</option>');
    });
    
    $('#filter_status').on('change', function(){
        dataTable.column("status:name").search($(this).val()).draw();
    });
    
    //Tree user filter
    $(filters).append('<select id="filter_user" />');
    $("#filter_user").append('<option value="">User</option>');
    
    //AJAX request to get users
    $.get('https://digital.wyndham.vic.gov.au/treeplanting/getListOfUsers', function(data){
	    $.each($.parseJSON(data), function(k, v){
		    if(v.status_changed_user != null)
			    $("#filter_user").append('<option value="' + v.status_changed_user + '">' + v.status_changed_user + '</option>');
	    });
    });
    
    $('#filter_user').on('change', function(){
        dataTable.column("user:name").search($(this).val()).draw();
    });
    
    //Tree date filter
    $(filters).append('<select id="filter_date" />');
    $("#filter_date").append('<option value="">All Time</option>');
    $("#filter_date").append('<option value="week">The Last Week</option>');
    $("#filter_date").append('<option value="month">The Last Month</option>');
    
    $("#filter_date").on("change", function(){
	    if($(this).val()){
	    	dataTable.ajax.url('https://digital.wyndham.vic.gov.au/treeplanting/getListOfTreesInDateRange/' + $(this).val());
	    } else {
		    dataTable.ajax.url('https://digital.wyndham.vic.gov.au/treeplanting/getListOfTrees');
	    }
	    dataTable.ajax.reload();
    });
});