L.Control.createMarkerButton = L.Control.extend({
	onAdd: function(map) {
		var wmaps = new WyndhamMaps();
		var button = L.DomUtil.create('div');
		$(button).addClass("leaflet-control-layers");
		if(window.devicePixelRatio > 1){
			$(button).html("<i style='top:1px;position:relative' class='fa fa-crosshairs'></i>");
			$(button).css('width', '48px');
			$(button).css('height', '48px');
		} else {
			$(button).html("<i style='top:-3px;position:relative' class='fa fa-crosshairs'></i>");
			$(button).css('width', '36px');
			$(button).css('height', '36px');
		}
		$(button).css('cursor', 'pointer');
		$(button).css('font-size', '28px');
		$(button).css('text-align', 'center');
		L.DomEvent.on(button, 'click', function(){
			var self = this;
			var id;
			
			var icon = L.icon({
				iconUrl: 'https://cdn.rawgit.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-blue.png',
				shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
				iconSize: [25, 41],
				iconAnchor: [12, 41],
				popupAnchor: [1, -34],
				shadowSize: [41, 41]
			});
			
			$.get('https://digital.wyndham.vic.gov.au/treeplanting/getNextTreeID', function(id){
				console.log(id);
				data = map.getCenter().lat + "|" + map.getCenter().lng + "|" + id;
				$.get('https://digital.wyndham.vic.gov.au/treeplanting/createNewMarker/' + data);
				var newMarker = L.marker(map.getCenter(), {
					'icon': icon,
					'draggable': true,
					'properties' : {
						'id' : id,
						'previous_botanical_name': '',
						'previous_common_name': '',
						'current_botanical_name': '',
						'current_common_name': '',
						'comments': '',
						'is_planted': false
					},
				}).on('click', function(e){
					wmaps.showForm(this.options.properties, this.getLatLng(), this);
				}).addTo(map);
				
				wmaps.showForm(newMarker.options.properties, newMarker.getLatLng(), this);
			});			
		});
		
		return button;
	},
	onRemove: function(map){
		
	}
})

L.control.createMarkerButton = function(opts) {
	return new L.Control.createMarkerButton(opts);
}

class WyndhamMaps {
    constructor() {
        this.map = null;
        this.layersToLoad = [];
        this.layerTypes = {
            PROPOSED : 'proposed',
            Planted : 'planted',
        }
        this.markerLayer = null;
    }
	
	getCommonNames(current) {
		var common_names = "";
		$.get('https://digital.wyndham.vic.gov.au/treeplanting/getTreeNameArray', function(data){ 
			$.each($.parseJSON(data), function(key, value){
				common_names += "<option value=\"" + value['tree_type_id'] + "\">" + value['tree_common'] + "</option>";
				if(key == $.parseJSON(data).length - 1){
					/*$("#common_name").select2({
						dropdownParent: $('.popupForm')
					});*/
					$("#common_name").html(common_names);
					var req = "common|" + encodeURI(current);
					$.get(
						'https://digital.wyndham.vic.gov.au/treeplanting/getTreeNameIDFromName/' + req,
						function(comID){
							if(comID){
								$("#common_name").val(comID);
							} else {
								$("#common_name").val(current);
							}
						}
					);
				}
			});
		});
	}
	
	getBotanicalNames(current) {
		var botanical_names = "";
		$.get('https://digital.wyndham.vic.gov.au/treeplanting/getTreeNameArray', function(data){ 
			$.each($.parseJSON(data), function(key, value){
				botanical_names += "<option value=\"" + value['tree_type_id'] + "\">" + value['tree_botanical'] + "</option>";
				if(key == $.parseJSON(data).length - 1){
					/*$("#botanical_name").select2({
						dropdownParent: $('.popupForm')
					});*/
					$("#botanical_name").html(botanical_names);
					var req = "botanical|" + encodeURIComponent(current);
					$.get(
						'https://digital.wyndham.vic.gov.au/treeplanting/getTreeNameIDFromName/' + req,
						function(botID){
							if(botID){
								$("#botanical_name").val(botID);
							} else {
								$("#botanical_name").val(current);
							}
						}
					);
				}
			});
		});
	}
	
    /* initialises the map with our custom map imagery / terrain layers */
    initMap() {
        var imgtitle = '';
        var imageryobj = {
            "1": {
                "Title": "Aerial Imagery)",
                "Type": "WMTS",
                "URL": "https:\/\/imagery.wyndham.vic.gov.au\/erdas-iws\/ogc\/wmts\/Aerial?service=WMTS&request=getcapabilities",
                "Leaflet_URL": "https:\/\/imagery.wyndham.vic.gov.au\/erdas-iws\/ogc\/wms\/Aerial?service=WMTS&request=getcapabilities",
                "iOS_URL": "https:\/\/imagery.wyndham.vic.gov.au\/erdas-iws\/ogc\/wmts\/Aerial?service=WMTS&TileMatrixSet=googlemapscompatibleext2:epsg:3857&TileMatrix=\\()&VERSION=1.0.0&style=default&LAYER=2016-05-21&request=GetTile&FORMAT=image\/jpeg&TileRow=\\()&TileCol=\\()",
                "Layer": "2017-01-27",
                "Latest": 1
            }/*,
            "2": {
                "Title": "2016-10",
                "Type": "WMTS",
                "URL": "https:\/\/imagery.wyndham.vic.gov.au\/erdas-iws\/ogc\/wmts\/Aerial?service=WMTS&request=getcapabilities",
                "Leaflet_URL": "https:\/\/imagery.wyndham.vic.gov.au\/erdas-iws\/ogc\/wms\/Aerial?service=WMTS&request=getcapabilities",
                "iOS_URL": "https:\/\/imagery.wyndham.vic.gov.au\/erdas-iws\/ogc\/wmts\/Aerial?service=WMTS&TileMatrixSet=googlemapscompatibleext2:epsg:3857&TileMatrix=\\()&VERSION=1.0.0&style=default&LAYER=2016-05-21&request=GetTile&FORMAT=image\/jpeg&TileRow=\\()&TileCol=\\()",
                "Layer": "2016-10-14",
                "Latest": 0
            },
            "3": {
                "Title": "2016-05-21",
                "Type": "WMTS",
                "URL": "https:\/\/imagery.wyndham.vic.gov.au\/erdas-iws\/ogc\/wmts\/Aerial?service=WMTS&request=getcapabilities",
                "Leaflet_URL": "https:\/\/imagery.wyndham.vic.gov.au\/erdas-iws\/ogc\/wms\/Aerial?service=WMTS&request=getcapabilities",
                "iOS_URL": "https:\/\/imagery.wyndham.vic.gov.au\/erdas-iws\/ogc\/wmts\/Aerial?service=WMTS&TileMatrixSet=googlemapscompatibleext2:epsg:3857&TileMatrix=\\()&VERSION=1.0.0&style=default&LAYER=2016-05-21&request=GetTile&FORMAT=image\/jpeg&TileRow=\\()&TileCol=\\()",
                "Layer": "2016-05-21",
                "Latest": 0
            },
            "4": {
                "Title": "2016-02 (includes Melton)",
                "Type": "WMTS",
                "URL": "https:\/\/imagery.wyndham.vic.gov.au\/erdas-iws\/ogc\/wmts\/Aerial?service=WMTS&request=getcapabilities",
                "Leaflet_URL": "https:\/\/imagery.wyndham.vic.gov.au\/erdas-iws\/ogc\/wms\/Aerial?service=WMTS&request=getcapabilities",
                "iOS_URL": "https:\/\/imagery.wyndham.vic.gov.au\/erdas-iws\/ogc\/wmts\/Aerial?service=WMTS&TileMatrixSet=googlemapscompatibleext2:epsg:3857&TileMatrix=\\()&VERSION=1.0.0&style=default&LAYER=2016-02-05&request=GetTile&FORMAT=image\/jpeg&TileRow=\\()&TileCol=\\()",
                "Layer": "2016-02-05",
                "Latest": 0
            },
            "5": {
                "Title": "2015-10",
                "Type": "WMTS",
                "URL": "https:\/\/imagery.wyndham.vic.gov.au\/erdas-iws\/ogc\/wmts\/Aerial?service=WMTS&request=getcapabilities",
                "Leaflet_URL": "https:\/\/imagery.wyndham.vic.gov.au\/erdas-iws\/ogc\/wms\/Aerial?service=WMTS&request=getcapabilities",
                "iOS_URL": "https:\/\/imagery.wyndham.vic.gov.au\/erdas-iws\/ogc\/wmts\/Aerial?service=WMTS&TileMatrixSet=googlemapscompatibleext2:epsg:3857&TileMatrix=\\()&VERSION=1.0.0&style=default&LAYER=2015-10-15&request=GetTile&FORMAT=image\/jpeg&TileRow=\\()&TileCol=\\()",
                "Layer": "2015-10-15",
                "Latest": 0
            },
            "6": {
                "Title": "2015-02",
                "Type": "WMTS",
                "URL": "https:\/\/imagery.wyndham.vic.gov.au\/erdas-iws\/ogc\/wmts\/Aerial?service=WMTS&request=getcapabilities",
                "Leaflet_URL": "https:\/\/imagery.wyndham.vic.gov.au\/erdas-iws\/ogc\/wms\/Aerial?service=WMTS&request=getcapabilities",
                "iOS_URL": "https:\/\/imagery.wyndham.vic.gov.au\/erdas-iws\/ogc\/wmts\/Aerial?service=WMTS&TileMatrixSet=googlemapscompatibleext2:epsg:3857&TileMatrix=\\()&VERSION=1.0.0&style=default&LAYER=2015-02&request=GetTile&FORMAT=image\/jpeg&TileRow=\\()&TileCol=\\()",
                "Layer": "2015-02",
                "Latest": 0
            },
            "7": {
                "Title": "2014-10",
                "Type": "WMTS",
                "URL": "https:\/\/imagery.wyndham.vic.gov.au\/erdas-iws\/ogc\/wmts\/Aerial?service=WMTS&request=getcapabilities",
                "Leaflet_URL": "https:\/\/imagery.wyndham.vic.gov.au\/erdas-iws\/ogc\/wms\/Aerial?service=WMTS&request=getcapabilities",
                "iOS_URL": "https:\/\/imagery.wyndham.vic.gov.au\/erdas-iws\/ogc\/wmts\/Aerial?service=WMTS&TileMatrixSet=googlemapscompatibleext2:epsg:3857&TileMatrix=\\()&VERSION=1.0.0&style=default&LAYER=2014-10-17&request=GetTile&FORMAT=image\/jpeg&TileRow=\\()&TileCol=\\()",
                "Layer": "2014-10-17",
                "Latest": 0
            },
            "8": {
                "Title": "2013-12",
                "Type": "WMTS",
                "URL": "https:\/\/imagery.wyndham.vic.gov.au\/erdas-iws\/ogc\/wmts\/Aerial?service=WMTS&request=getcapabilities",
                "Leaflet_URL": "https:\/\/imagery.wyndham.vic.gov.au\/erdas-iws\/ogc\/wms\/Aerial?service=WMTS&request=getcapabilities",
                "iOS_URL": "https:\/\/imagery.wyndham.vic.gov.au\/erdas-iws\/ogc\/wmts\/Aerial?service=WMTS&TileMatrixSet=googlemapscompatibleext2:epsg:3857&TileMatrix=\\()&VERSION=1.0.0&style=default&LAYER=2013-12&request=GetTile&FORMAT=image\/jpeg&TileRow=\\()&TileCol=\\()",
                "Layer": "2013-12",
                "Latest": 0
            },
            "9": {
                "Title": "2013-01",
                "Type": "WMTS",
                "URL": "https:\/\/imagery.wyndham.vic.gov.au\/erdas-iws\/ogc\/wmts\/Aerial?service=WMTS&request=getcapabilities",
                "Leaflet_URL": "https:\/\/imagery.wyndham.vic.gov.au\/erdas-iws\/ogc\/wms\/Aerial?service=WMTS&request=getcapabilities",
                "iOS_URL": "https:\/\/imagery.wyndham.vic.gov.au\/erdas-iws\/ogc\/wmts\/Aerial?service=WMTS&TileMatrixSet=googlemapscompatibleext2:epsg:3857&TileMatrix=\\()&VERSION=1.0.0&style=default&LAYER=2013-01&request=GetTile&FORMAT=image\/jpeg&TileRow=\\()&TileCol=\\()",
                "Layer": "2013-01",
                "Latest": 0
            },
            "10": {
                "Title": "2011-12",
                "Type": "WMTS",
                "URL": "https:\/\/imagery.wyndham.vic.gov.au\/erdas-iws\/ogc\/wmts\/Aerial?service=WMTS&request=getcapabilities",
                "Leaflet_URL": "https:\/\/imagery.wyndham.vic.gov.au\/erdas-iws\/ogc\/wms\/Aerial?service=WMTS&request=getcapabilities",
                "iOS_URL": "https:\/\/imagery.wyndham.vic.gov.au\/erdas-iws\/ogc\/wmts\/Aerial?service=WMTS&TileMatrixSet=googlemapscompatibleext2:epsg:3857&TileMatrix=\\()&VERSION=1.0.0&style=default&LAYER=2011-12&request=GetTile&FORMAT=image\/jpeg&TileRow=\\()&TileCol=\\()",
                "Layer": "2011-12",
                "Latest": 0
            },
            "11": {
                "Title": "2010-12",
                "Type": "WMTS",
                "URL": "https:\/\/imagery.wyndham.vic.gov.au\/erdas-iws\/ogc\/wmts\/Aerial?service=WMTS&request=getcapabilities",
                "Leaflet_URL": "https:\/\/imagery.wyndham.vic.gov.au\/erdas-iws\/ogc\/wms\/Aerial?service=WMTS&request=getcapabilities",
                "iOS_URL": "https:\/\/imagery.wyndham.vic.gov.au\/erdas-iws\/ogc\/wmts\/Aerial?service=WMTS&TileMatrixSet=googlemapscompatibleext2:epsg:3857&TileMatrix=\\()&VERSION=1.0.0&style=default&LAYER=2010-12&request=GetTile&FORMAT=image\/jpeg&TileRow=\\()&TileCol=\\()",
                "Layer": "2010-12",
                "Latest": 0
            },
            "12": {
                "Title": "2010-01",
                "Type": "WMTS",
                "URL": "https:\/\/imagery.wyndham.vic.gov.au\/erdas-iws\/ogc\/wmts\/Aerial?service=WMTS&request=getcapabilities",
                "Leaflet_URL": "https:\/\/imagery.wyndham.vic.gov.au\/erdas-iws\/ogc\/wms\/Aerial?service=WMTS&request=getcapabilities",
                "iOS_URL": "https:\/\/imagery.wyndham.vic.gov.au\/erdas-iws\/ogc\/wmts\/Aerial?service=WMTS&TileMatrixSet=googlemapscompatibleext2:epsg:3857&TileMatrix=\\()&VERSION=1.0.0&style=default&LAYER=2010-01-20&request=GetTile&FORMAT=image\/jpeg&TileRow=\\()&TileCol=\\()",
                "Layer": "2010-01-20",
                "Latest": 0
            },
            "13": {
                "Title": "2009-11",
                "Type": "WMTS",
                "URL": "https:\/\/imagery.wyndham.vic.gov.au\/erdas-iws\/ogc\/wmts\/Aerial?service=WMTS&request=getcapabilities",
                "Leaflet_URL": "https:\/\/imagery.wyndham.vic.gov.au\/erdas-iws\/ogc\/wms\/Aerial?service=WMTS&request=getcapabilities",
                "iOS_URL": "https:\/\/imagery.wyndham.vic.gov.au\/erdas-iws\/ogc\/wmts\/Aerial?service=WMTS&TileMatrixSet=googlemapscompatibleext2:epsg:3857&TileMatrix=\\()&VERSION=1.0.0&style=default&LAYER=2009-11-01&request=GetTile&FORMAT=image\/jpeg&TileRow=\\()&TileCol=\\()",
                "Layer": "2009-11-01",
                "Latest": 0
            },
            "14": {
                "Title": "2007-10",
                "Type": "WMTS",
                "URL": "https:\/\/imagery.wyndham.vic.gov.au\/erdas-iws\/ogc\/wmts\/Aerial?service=WMTS&request=getcapabilities",
                "Leaflet_URL": "https:\/\/imagery.wyndham.vic.gov.au\/erdas-iws\/ogc\/wms\/Aerial?service=WMTS&request=getcapabilities",
                "iOS_URL": "https:\/\/imagery.wyndham.vic.gov.au\/erdas-iws\/ogc\/wmts\/Aerial?service=WMTS&TileMatrixSet=googlemapscompatibleext2:epsg:3857&TileMatrix=\\()&VERSION=1.0.0&style=default&LAYER=2007-10-20&request=GetTile&FORMAT=image\/jpeg&TileRow=\\()&TileCol=\\()",
                "Layer": "2007-10-20",
                "Latest": 0
            },
            "15": {
                "Title": "2006-01",
                "Type": "WMTS",
                "URL": "https:\/\/imagery.wyndham.vic.gov.au\/erdas-iws\/ogc\/wmts\/Aerial?service=WMTS&request=getcapabilities",
                "Leaflet_URL": "https:\/\/imagery.wyndham.vic.gov.au\/erdas-iws\/ogc\/wms\/Aerial?service=WMTS&request=getcapabilities",
                "iOS_URL": "https:\/\/imagery.wyndham.vic.gov.au\/erdas-iws\/ogc\/wmts\/Aerial?service=WMTS&TileMatrixSet=googlemapscompatibleext2:epsg:3857&TileMatrix=\\()&VERSION=1.0.0&style=default&LAYER=2006-01-24&request=GetTile&FORMAT=image\/jpeg&TileRow=\\()&TileCol=\\()",
                "Layer": "2006-01-24",
                "Latest": 0
            },
            "16": {
                "Title": "2005-12",
                "Type": "WMTS",
                "URL": "https:\/\/imagery.wyndham.vic.gov.au\/erdas-iws\/ogc\/wmts\/Aerial?service=WMTS&request=getcapabilities",
                "Leaflet_URL": "https:\/\/imagery.wyndham.vic.gov.au\/erdas-iws\/ogc\/wms\/Aerial?service=WMTS&request=getcapabilities",
                "iOS_URL": "https:\/\/imagery.wyndham.vic.gov.au\/erdas-iws\/ogc\/wmts\/Aerial?service=WMTS&TileMatrixSet=googlemapscompatibleext2:epsg:3857&TileMatrix=\\()&VERSION=1.0.0&style=default&LAYER=2005-12-11&request=GetTile&FORMAT=image\/jpeg&TileRow=\\()&TileCol=\\()",
                "Layer": "2005-12-11",
                "Latest": 0
            }
            */
        };
        
        var baseLayers = {};
        var imgkey;
        var i = 1;
        
        /* there are different imagery layers taken during the years. these are included in imageryobj */
        for (var key in imageryobj) {
            if (imageryobj.hasOwnProperty(key)) { 
                //check tile type to update image variable, there are duplicated variable because it causes issues having the same layer selected between the maps.
                if (imageryobj[key].Type == 'TS') {
                    var image = new L.tileLayer(imageryobj[key].Leaflet_URL,{
                        layers: imageryobj[key].Layer,
                        minZoom: 1,
                        maxZoom:21,
                    });
                } else {
                    var image = new L.tileLayer.wms(imageryobj[key].Leaflet_URL,{
                        layers: imageryobj[key].Layer,
                        minZoom: 1,
                        maxZoom:21,
                    });
                }
                if (i==1) {imgkey=imageryobj[key].Title;}
                baseLayers[imageryobj[key].Title] = image;
            }
            i=i+1;
        }

        var base_lyr = new L.tileLayer('https://imagery.wyndham.vic.gov.au/tiles/base/{z}/{x}/{y}.png',{
            maxNativeZoom:20,
            maxZoom: 21,
            minZoom: 11,
        });
        
        baseLayers['Terrain'] = base_lyr;
        
        var label_lyr = new L.tileLayer('https://imagery.wyndham.vic.gov.au/tiles/road_labels/{z}/{x}/{y}.png',{
            maxNativeZoom:20,
            maxZoom: 21,
            minZoom: 11,
        });
        
        var property_lyr = new L.tileLayer('https://imagery.wyndham.vic.gov.au/tiles/property_aerial/{z}/{x}/{y}.png',{
            maxNativeZoom:20,
            maxZoom: 21,
            minZoom: 16,
        });
        
        var overlays = {
            "Labels": label_lyr,
            "Property": property_lyr
        };    

        // create map object
        var map = L.map('map',  {
            center:[-37.877699, 144.681702],
            zoom:14,
            minZoom:11,
            maxZoom:19,
            layers: [baseLayers['Terrain'], label_lyr, property_lyr]
        });

        L.control.scale({position:'bottomleft', maxWidth:100, metric:true, imperial:false}).addTo(map);

        var control = L.control.layers(baseLayers,overlays);
        control.addTo(map);
        
        L.control.createMarkerButton({position:'topright'}).addTo(map);
        
        this.map = map;
        
        //We have to define the crosshair outside of the UI utilities because there's no option for an element to be centred within the map
        
        var crosshair = document.createElement('div');
		$(crosshair).css({
			'filter': 'invert',
			'position': 'absolute',
			'width': '15px',
			'height': '15px',
			'top': '50%',
			'left': '50%',
			'transform': 'translate(-10px, -10px)',
			'background-color': '#00AEEF',
			'border-radius': '10px',
			'border': '1px solid #fff',
			'z-index': 9997,
			'pointer-events': 'none',
			'box-shadow': '0px 0px 5px 5px rgb(180, 217, 231)'
		});
		$("#map").append(crosshair);
		
		/*
			Set up our list pane
		*/
		$("#listPane").append("<h1>&nbsp;Tree Inspect</h1>");
		$("#listPane").append("<ul id=\"listOfTreesInView\"></ul>");
    }; 

    testMessage() {
        console.log('this is a test message');
    }

    onEachFeature(feature, layer) {
        var popupContent = "";

        if (feature.properties && feature.properties.near_address) {
            popupContent += feature.properties.near_address;
        }

        layer.bindPopup(popupContent);
    }
	
	getBounds() {
		return this.map.getBounds();
	}
	
	addNewMarkeratCursor(map) {

	}
	
	addMarkers(json) {
		var self = this;
		
		var marker = {
			planted: L.icon({
				iconUrl: 'https://cdn.rawgit.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-green.png',
				shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
				iconSize: [25, 41],
				iconAnchor: [12, 41],
				popupAnchor: [1, -34],
				shadowSize: [41, 41]
			}),
			ready: L.icon({
				iconUrl: 'https://cdn.rawgit.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-orange.png',
				shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
				iconSize: [25, 41],
				iconAnchor: [12, 41],
				popupAnchor: [1, -34],
				shadowSize: [41, 41]
			}),
			unsuitable: L.icon({
				iconUrl: 'https://cdn.rawgit.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-red.png',
				shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
				iconSize: [25, 41],
				iconAnchor: [12, 41],
				popupAnchor: [1, -34],
				shadowSize: [41, 41]
			}),
			unchecked: L.icon({
				iconUrl: 'https://cdn.rawgit.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-blue.png',
				shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
				iconSize: [25, 41],
				iconAnchor: [12, 41],
				popupAnchor: [1, -34],
				shadowSize: [41, 41]
			})
		}
		
		if(this.markerLayer){
			this.markerLayer.clearLayers();
			$("#listOfTreesInView").html("");
		}
		
		var markers = L.markerClusterGroup();
		self.markerLayer = L.geoJSON(json, {
			pointToLayer: function (feature, latlng){
				var icn;
				
				switch(feature.properties.status){
					case 'planted':
						icn = marker.planted;
						break;
						
					case 'ready':
						icn = marker.ready;
						break;
						
					case 'unsuitable':
						icn = marker.unsuitable;
						break;
						
					case 'unchecked':
						icn = marker.unchecked;
						break;
						
					default:
						icn = marker.unchecked;
				}
				
				return markers.addLayer(
					L.marker(latlng, {
						icon: icn,
						draggable: true,
						properties: feature.properties
					}).on('click', function(e){
						self.showForm(this.options.properties, this.getLatLng(), this);
					}).on('moveend', function(e){
						var data = this.options.properties.id + ",";
						data += this.getLatLng().lat + ",";
						data += this.getLatLng().lng;
						$.get(
							'https://digital.wyndham.vic.gov.au/treeplanting/updateMarkerPosition/' + data,
						);
						self.showForm(this.options.properties, this.getLatLng(), this);
					}).on('mousedown', function(e){
						/*var _self = this;
						console.log("predrag");
						_self.dragging.disable();
						var holdTime = 500;
						setTimeout(function(){
							console.log("draggable");
							_self.dragging.enable();
						}, holdTime);*/
					})
				);
			}
		}).addTo(this.map);
	}
	
	showForm(properties, latlng, marker, mandatoryFields = []){
		var self = this;
		
		$.getJSON('https://digital.wyndham.vic.gov.au/treeplanting/getMarkerMetadata/' + properties.id, function(properties){
			//Marker definitions object
			var markers = {
				planted: L.icon({
					iconUrl: 'https://cdn.rawgit.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-green.png',
					shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
					iconSize: [25, 41],
					iconAnchor: [12, 41],
					popupAnchor: [1, -34],
					shadowSize: [41, 41]
				}),
				ready: L.icon({
					iconUrl: 'https://cdn.rawgit.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-orange.png',
					shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
					iconSize: [25, 41],
					iconAnchor: [12, 41],
					popupAnchor: [1, -34],
					shadowSize: [41, 41]
				}),
				unsuitable: L.icon({
					iconUrl: 'https://cdn.rawgit.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-red.png',
					shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
					iconSize: [25, 41],
					iconAnchor: [12, 41],
					popupAnchor: [1, -34],
					shadowSize: [41, 41]
				}),
				unchecked: L.icon({
					iconUrl: 'https://cdn.rawgit.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-blue.png',
					shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
					iconSize: [25, 41],
					iconAnchor: [12, 41],
					popupAnchor: [1, -34],
					shadowSize: [41, 41]
				})
			}
			
			//The blanking background
			var popupBg = document.createElement("div");
			$(popupBg).addClass("popupBg");
			$(popupBg).on("click", function(){
				$(this).remove();
			});
			
			//The Form
			var formDiv = document.createElement("div");
			$(formDiv).addClass("popupForm");
			$(formDiv).on("click", function(e){
				e.stopPropagation();
			});
			
			//Heading
			var heading = document.createElement("h3");
			heading.innerHTML = "Tree Information";
			
			//The beginnings of the form
			var form = document.createElement("div");
			$(form).addClass("form");
			
			//ID
			var idHidden = document.createElement("input");
			idHidden.type = "hidden";
			idHidden.name = "id";
			idHidden.id = "id";
			idHidden.value = properties.id;
			form.appendChild(idHidden);
			
			//Address
			form.innerHTML += "Nearest Address:";
			var addressInput = document.createElement("input");
			addressInput.name = "address";
			addressInput.id = "address";
			$(addressInput).attr("value", "Locating...");
			form.appendChild(addressInput);
			
			if(properties.nearest_address){
				$(addressInput).attr("value", properties.nearest_address);
			} else {
				$.get("https://digital.wyndham.vic.gov.au/treeplanting/getNearestAddress/" + latlng.lat + "|" + latlng.lng, function(resp){
					$(addressInput).attr("value", resp);
				});
			}
			
			//Loading placeholder
			var loadingPlaceholder = document.createElement("option");
			loadingPlaceholder.innerHTML = "Loading...";
			
			//Botanical Name
			form.innerHTML += "Botanical Name:"
			var botanicalHidden = document.createElement("input");
			botanicalHidden.type = "hidden";
			botanicalHidden.name = "ex_botanical_name";
			botanicalHidden.id = "ex_botanical_name";
			botanicalHidden.value = properties.botanical_name;
			form.appendChild(botanicalHidden);
			
			var botanicalInput = document.createElement("input");
			botanicalInput.type = "text";
			botanicalInput.list = "botanical_name";
			botanicalInput.name = "botanical_name";
			botanicalInput.id = "botanical_name";
			botanicalInput.appendChild(loadingPlaceholder);
			form.appendChild(botanicalInput);
			self.getBotanicalNames(properties.botanical_name);
			
			//Common Name
			form.innerHTML += "Common Name:"
			var commonHidden = document.createElement("input");
			commonHidden.type = "hidden";
			commonHidden.name = "ex_common_name";
			commonHidden.id = "ex_common_name";
			commonHidden.value = properties.common_name;
			form.appendChild(commonHidden);
			
			var commonInput = document.createElement("input");
			commonInput.type = "text";
			commonInput.list = "common_name";
			commonInput.name = "common_name";
			commonInput.id = "common_name";
			commonInput.appendChild(loadingPlaceholder);
			form.appendChild(commonInput);
			self.getCommonNames(properties.common_name);
			
			//Comments
			form.innerHTML += "Comments:";
			var commentTextArea = document.createElement("textarea");
			commentTextArea.name = "comments";
			commentTextArea.id = "comments";
			form.appendChild(commentTextArea);
			
			if(properties.comments){
				$(commentTextArea).html(properties.comments);
			}
			
			//Status Selector
			var statusSelectorHidden = document.createElement("input");
			statusSelectorHidden.type = "hidden";
			statusSelectorHidden.name = "status";
			statusSelectorHidden.id = "status";
			if(properties.status){
				statusSelectorHidden.value = properties.status;
			} else {
				statusSelectorHidden.value = "unchecked";
			}
			form.appendChild(statusSelectorHidden);
			
			var statusSelectorDiv = document.createElement("div");
			statusSelectorDiv.className = "status-radio";
			
			var buttons = [
				{
					"id": "unchecked",
					"symbol": "fa fa-question-circle fa-4x"
				},
				{
					"id": "unsuitable",
					"symbol": "fa fa-ban fa-4x"
				},
				{
					"id": "ready",
					"symbol": "fa fa-check fa-4x"
				},
				{
					"id": "planted",
					"symbol": "fa fa-tree fa-4x"
				}
			];
			
			$.each(buttons, function(key, value){
				var radioButtonDiv = document.createElement("div");
				if(statusSelectorHidden.value == value.id){
					radioButtonDiv.className = "status-radio-button active";
				} else {
					radioButtonDiv.className = "status-radio-button";	
				}
				radioButtonDiv.id = value.id;
				
				var radioButtonSymbol = document.createElement("i");
				radioButtonSymbol.className = value.symbol;
				
				radioButtonDiv.appendChild(radioButtonSymbol);
				statusSelectorDiv.appendChild(radioButtonDiv);
			});
			
			form.appendChild(statusSelectorDiv);
			
			if(properties.status_changed != null){
				form.innerHTML += "<br /><small>Last updated on " + properties.status_changed + " by %%USER%%</small>";
			} else {
				form.innerHTML += "<br /><small>Last updated never by nobody</small>";
			}
			
			var submitButton = document.createElement("button");
			submitButton.innerHTML = "Save";
			
			$(submitButton).on('click', function(){
				/*var id = idHidden.value;
				var data = new Object;
				
				data['previous_botanical_name'] = properties.botanical_name;
				data['botanical_name'] = $("#botanical_name").val();
				data['previous_common_name'] = properties.common_name;
				data['common_name'] = $("#common_name").val();
				data['comments'] = $("#comments").val();
				data['address'] = $("#address").val();
				
				console.log(data);
				
				var reqString = "";
				$.each(data, function(k, v){
					reqString += v + "|";
				});
				
				$.get(
					'https://digital.wyndham.vic.gov.au/treeplanting/updateMarkerMetadata/' + reqString,
				);*/
			});
			
			form.appendChild(submitButton);
			
			$(formDiv).append(heading);
			$(formDiv).append(form);
			
			$("body").append(popupBg);
			$(popupBg).append(formDiv);
			
			$(".status-radio-button").on('click', function(){
				$(".status-radio-button").removeClass('active');
				$(this).addClass('active');
				$("#status").val($(this).prop("id"));
				$.get("https://digital.wyndham.vic.gov.au/treeplanting/updateTreeStatus/" + properties.id + "|" + $("#status").val());
				switch($("#status").val()){
					case 'unchecked':
						marker.setIcon(markers.unchecked);
						break;
						
					case 'unsuitable':
						marker.setIcon(markers.unsuitable);
						break;
						
					case 'ready':
						marker.setIcon(markers.ready);
						break;
						
					case 'planted':
						marker.setIcon(markers.planted);
						break;
						
					default:
						marker.setIcon(markers.unchecked);
				}
			});
			
			$("#common_name, #botanical_name").on("change", function(){
				var value = $(this).val();
				$("#common_name").val(value);
				$("#botanical_name").val(value);
			});
		});
	}
	
    /* Layer type is the actual */
    createLayerForType(layerType) {
        // var req = -37.8782831086417+','+-37.891023983938+','+144.680405585268+','+144.700885230972;

        // var markers = (function() {
        //     var json = null;
        //     $.ajax({
        //         'async': false,
        //         'global': false,
        //         'url': "/getGeoJson/"+req,
        //         'dataType': "json",
        //         'success': function (data) {
        //           json = data;
        //         }
        //     });
        //     return json;
        // })();

        // var geojson = L.geoJSON([markers], {

        //     style: function (feature) {
        //       return feature.properties && feature.properties.style;
        //     },

        //     onEachFeature: this.onEachFeature,

        //     pointToLayer: function (feature, latlng) {
        //       return L.marker(latlng);
        //     }
        // });

        // var markers = L.markerClusterGroup();
        // markers.addLayer(geojson);

        // map.addLayer(markers);

        // finish to load cluster
    }
}