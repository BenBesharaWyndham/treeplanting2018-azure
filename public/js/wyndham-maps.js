L.Control.createMarkerButton = L.Control.extend({
	onAdd: function(map) {
		var wmaps = new WyndhamMaps();
		var button = L.DomUtil.create('div');
		$(button).addClass("leaflet-control-layers");
		if(window.devicePixelRatio > 1){
			$(button).html("<i style='top:1px;position:relative' class='fa fa-crosshairs'></i>");
			$(button).css('width', '48px');
			$(button).css('height', '48px');
		} else {
			$(button).html("<i style='top:-3px;position:relative' class='fa fa-crosshairs'></i>");
			$(button).css('width', '48px');
			$(button).css('height', '48px');
		}
		$(button).css('cursor', 'pointer');
		$(button).css('font-size', '28px');
		$(button).css('text-align', 'center');
		L.DomEvent.on(button, 'click', function(){
			var self = this;
			var id;
			
			var icon = L.icon({
				iconUrl: 'https://cdn.rawgit.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-blue.png',
				shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
				iconSize: [25, 41],
				iconAnchor: [12, 41],
				popupAnchor: [1, -34],
				shadowSize: [41, 41]
			});
			
			$.get('https://digital.wyndham.vic.gov.au/treeplanting/getNextTreeID', function(id){
				data = map.getCenter().lat + "|" + map.getCenter().lng + "|" + id;
				$.get('https://digital.wyndham.vic.gov.au/treeplanting/createNewMarker/' + data);
				var newMarker = L.marker(map.getCenter(), {
					'icon': icon,
					'draggable': true,
					'properties' : {
						'id' : id,
						'previous_botanical_name': '',
						'previous_common_name': '',
						'current_botanical_name': '',
						'current_common_name': '',
						'comments': '',
						'is_planted': false
					},
				}).on('click', function(e){
					wmaps.showForm(this.options.properties, this.getLatLng(), this);
				}).addTo(map);
				
				wmaps.showForm(newMarker.options.properties, newMarker.getLatLng(), this);
			});			
		});
		
		return button;
	},
	onRemove: function(map){
		
	}
})

L.control.createMarkerButton = function(opts) {
	return new L.Control.createMarkerButton(opts);
}

class WyndhamMaps {
    constructor() {
		this.azureURI = "https://treeinspectphotosdev.blob.core.windows.net/";
        this.map = null;
        //this.layersToLoad = [];
        /*this.layerTypes = {
            PROPOSED : 'proposed',
            Planted : 'planted',
        }*/
        this.markerLayer = null;
        
        this.plantedGroup = L.markerClusterGroup({
			disableClusteringAtZoom: 19
		});
		
		this.readyGroup = L.markerClusterGroup({
			disableClusteringAtZoom: 19
		});
		
		this.unsuitableGroup = L.markerClusterGroup({
			disableClusteringAtZoom: 19
		});
		
		this.uncheckedGroup = L.markerClusterGroup({
			disableClusteringAtZoom: 19
		});
    }
		
    /* initialises the map with our custom map imagery / terrain layers */
    initMap() {
        var imgtitle = '';
        var imageryobj = {
            "1": {
                "Title": "Aerial Imagery)",
                "Type": "WMTS",
                "URL": "https:\/\/imagery.wyndham.vic.gov.au\/erdas-iws\/ogc\/wmts\/Aerial?service=WMTS&request=getcapabilities",
                "Leaflet_URL": "https:\/\/imagery.wyndham.vic.gov.au\/erdas-iws\/ogc\/wms\/Aerial?service=WMTS&request=getcapabilities",
                "iOS_URL": "https:\/\/imagery.wyndham.vic.gov.au\/erdas-iws\/ogc\/wmts\/Aerial?service=WMTS&TileMatrixSet=googlemapscompatibleext2:epsg:3857&TileMatrix=\\()&VERSION=1.0.0&style=default&LAYER=2016-05-21&request=GetTile&FORMAT=image\/jpeg&TileRow=\\()&TileCol=\\()",
                "Layer": "2017-01-27",
                "Latest": 1
            }/*,
            "2": {
                "Title": "2016-10",
                "Type": "WMTS",
                "URL": "https:\/\/imagery.wyndham.vic.gov.au\/erdas-iws\/ogc\/wmts\/Aerial?service=WMTS&request=getcapabilities",
                "Leaflet_URL": "https:\/\/imagery.wyndham.vic.gov.au\/erdas-iws\/ogc\/wms\/Aerial?service=WMTS&request=getcapabilities",
                "iOS_URL": "https:\/\/imagery.wyndham.vic.gov.au\/erdas-iws\/ogc\/wmts\/Aerial?service=WMTS&TileMatrixSet=googlemapscompatibleext2:epsg:3857&TileMatrix=\\()&VERSION=1.0.0&style=default&LAYER=2016-05-21&request=GetTile&FORMAT=image\/jpeg&TileRow=\\()&TileCol=\\()",
                "Layer": "2016-10-14",
                "Latest": 0
            },
            "3": {
                "Title": "2016-05-21",
                "Type": "WMTS",
                "URL": "https:\/\/imagery.wyndham.vic.gov.au\/erdas-iws\/ogc\/wmts\/Aerial?service=WMTS&request=getcapabilities",
                "Leaflet_URL": "https:\/\/imagery.wyndham.vic.gov.au\/erdas-iws\/ogc\/wms\/Aerial?service=WMTS&request=getcapabilities",
                "iOS_URL": "https:\/\/imagery.wyndham.vic.gov.au\/erdas-iws\/ogc\/wmts\/Aerial?service=WMTS&TileMatrixSet=googlemapscompatibleext2:epsg:3857&TileMatrix=\\()&VERSION=1.0.0&style=default&LAYER=2016-05-21&request=GetTile&FORMAT=image\/jpeg&TileRow=\\()&TileCol=\\()",
                "Layer": "2016-05-21",
                "Latest": 0
            },
            "4": {
                "Title": "2016-02 (includes Melton)",
                "Type": "WMTS",
                "URL": "https:\/\/imagery.wyndham.vic.gov.au\/erdas-iws\/ogc\/wmts\/Aerial?service=WMTS&request=getcapabilities",
                "Leaflet_URL": "https:\/\/imagery.wyndham.vic.gov.au\/erdas-iws\/ogc\/wms\/Aerial?service=WMTS&request=getcapabilities",
                "iOS_URL": "https:\/\/imagery.wyndham.vic.gov.au\/erdas-iws\/ogc\/wmts\/Aerial?service=WMTS&TileMatrixSet=googlemapscompatibleext2:epsg:3857&TileMatrix=\\()&VERSION=1.0.0&style=default&LAYER=2016-02-05&request=GetTile&FORMAT=image\/jpeg&TileRow=\\()&TileCol=\\()",
                "Layer": "2016-02-05",
                "Latest": 0
            },
            "5": {
                "Title": "2015-10",
                "Type": "WMTS",
                "URL": "https:\/\/imagery.wyndham.vic.gov.au\/erdas-iws\/ogc\/wmts\/Aerial?service=WMTS&request=getcapabilities",
                "Leaflet_URL": "https:\/\/imagery.wyndham.vic.gov.au\/erdas-iws\/ogc\/wms\/Aerial?service=WMTS&request=getcapabilities",
                "iOS_URL": "https:\/\/imagery.wyndham.vic.gov.au\/erdas-iws\/ogc\/wmts\/Aerial?service=WMTS&TileMatrixSet=googlemapscompatibleext2:epsg:3857&TileMatrix=\\()&VERSION=1.0.0&style=default&LAYER=2015-10-15&request=GetTile&FORMAT=image\/jpeg&TileRow=\\()&TileCol=\\()",
                "Layer": "2015-10-15",
                "Latest": 0
            },
            "6": {
                "Title": "2015-02",
                "Type": "WMTS",
                "URL": "https:\/\/imagery.wyndham.vic.gov.au\/erdas-iws\/ogc\/wmts\/Aerial?service=WMTS&request=getcapabilities",
                "Leaflet_URL": "https:\/\/imagery.wyndham.vic.gov.au\/erdas-iws\/ogc\/wms\/Aerial?service=WMTS&request=getcapabilities",
                "iOS_URL": "https:\/\/imagery.wyndham.vic.gov.au\/erdas-iws\/ogc\/wmts\/Aerial?service=WMTS&TileMatrixSet=googlemapscompatibleext2:epsg:3857&TileMatrix=\\()&VERSION=1.0.0&style=default&LAYER=2015-02&request=GetTile&FORMAT=image\/jpeg&TileRow=\\()&TileCol=\\()",
                "Layer": "2015-02",
                "Latest": 0
            },
            "7": {
                "Title": "2014-10",
                "Type": "WMTS",
                "URL": "https:\/\/imagery.wyndham.vic.gov.au\/erdas-iws\/ogc\/wmts\/Aerial?service=WMTS&request=getcapabilities",
                "Leaflet_URL": "https:\/\/imagery.wyndham.vic.gov.au\/erdas-iws\/ogc\/wms\/Aerial?service=WMTS&request=getcapabilities",
                "iOS_URL": "https:\/\/imagery.wyndham.vic.gov.au\/erdas-iws\/ogc\/wmts\/Aerial?service=WMTS&TileMatrixSet=googlemapscompatibleext2:epsg:3857&TileMatrix=\\()&VERSION=1.0.0&style=default&LAYER=2014-10-17&request=GetTile&FORMAT=image\/jpeg&TileRow=\\()&TileCol=\\()",
                "Layer": "2014-10-17",
                "Latest": 0
            },
            "8": {
                "Title": "2013-12",
                "Type": "WMTS",
                "URL": "https:\/\/imagery.wyndham.vic.gov.au\/erdas-iws\/ogc\/wmts\/Aerial?service=WMTS&request=getcapabilities",
                "Leaflet_URL": "https:\/\/imagery.wyndham.vic.gov.au\/erdas-iws\/ogc\/wms\/Aerial?service=WMTS&request=getcapabilities",
                "iOS_URL": "https:\/\/imagery.wyndham.vic.gov.au\/erdas-iws\/ogc\/wmts\/Aerial?service=WMTS&TileMatrixSet=googlemapscompatibleext2:epsg:3857&TileMatrix=\\()&VERSION=1.0.0&style=default&LAYER=2013-12&request=GetTile&FORMAT=image\/jpeg&TileRow=\\()&TileCol=\\()",
                "Layer": "2013-12",
                "Latest": 0
            },
            "9": {
                "Title": "2013-01",
                "Type": "WMTS",
                "URL": "https:\/\/imagery.wyndham.vic.gov.au\/erdas-iws\/ogc\/wmts\/Aerial?service=WMTS&request=getcapabilities",
                "Leaflet_URL": "https:\/\/imagery.wyndham.vic.gov.au\/erdas-iws\/ogc\/wms\/Aerial?service=WMTS&request=getcapabilities",
                "iOS_URL": "https:\/\/imagery.wyndham.vic.gov.au\/erdas-iws\/ogc\/wmts\/Aerial?service=WMTS&TileMatrixSet=googlemapscompatibleext2:epsg:3857&TileMatrix=\\()&VERSION=1.0.0&style=default&LAYER=2013-01&request=GetTile&FORMAT=image\/jpeg&TileRow=\\()&TileCol=\\()",
                "Layer": "2013-01",
                "Latest": 0
            },
            "10": {
                "Title": "2011-12",
                "Type": "WMTS",
                "URL": "https:\/\/imagery.wyndham.vic.gov.au\/erdas-iws\/ogc\/wmts\/Aerial?service=WMTS&request=getcapabilities",
                "Leaflet_URL": "https:\/\/imagery.wyndham.vic.gov.au\/erdas-iws\/ogc\/wms\/Aerial?service=WMTS&request=getcapabilities",
                "iOS_URL": "https:\/\/imagery.wyndham.vic.gov.au\/erdas-iws\/ogc\/wmts\/Aerial?service=WMTS&TileMatrixSet=googlemapscompatibleext2:epsg:3857&TileMatrix=\\()&VERSION=1.0.0&style=default&LAYER=2011-12&request=GetTile&FORMAT=image\/jpeg&TileRow=\\()&TileCol=\\()",
                "Layer": "2011-12",
                "Latest": 0
            },
            "11": {
                "Title": "2010-12",
                "Type": "WMTS",
                "URL": "https:\/\/imagery.wyndham.vic.gov.au\/erdas-iws\/ogc\/wmts\/Aerial?service=WMTS&request=getcapabilities",
                "Leaflet_URL": "https:\/\/imagery.wyndham.vic.gov.au\/erdas-iws\/ogc\/wms\/Aerial?service=WMTS&request=getcapabilities",
                "iOS_URL": "https:\/\/imagery.wyndham.vic.gov.au\/erdas-iws\/ogc\/wmts\/Aerial?service=WMTS&TileMatrixSet=googlemapscompatibleext2:epsg:3857&TileMatrix=\\()&VERSION=1.0.0&style=default&LAYER=2010-12&request=GetTile&FORMAT=image\/jpeg&TileRow=\\()&TileCol=\\()",
                "Layer": "2010-12",
                "Latest": 0
            },
            "12": {
                "Title": "2010-01",
                "Type": "WMTS",
                "URL": "https:\/\/imagery.wyndham.vic.gov.au\/erdas-iws\/ogc\/wmts\/Aerial?service=WMTS&request=getcapabilities",
                "Leaflet_URL": "https:\/\/imagery.wyndham.vic.gov.au\/erdas-iws\/ogc\/wms\/Aerial?service=WMTS&request=getcapabilities",
                "iOS_URL": "https:\/\/imagery.wyndham.vic.gov.au\/erdas-iws\/ogc\/wmts\/Aerial?service=WMTS&TileMatrixSet=googlemapscompatibleext2:epsg:3857&TileMatrix=\\()&VERSION=1.0.0&style=default&LAYER=2010-01-20&request=GetTile&FORMAT=image\/jpeg&TileRow=\\()&TileCol=\\()",
                "Layer": "2010-01-20",
                "Latest": 0
            },
            "13": {
                "Title": "2009-11",
                "Type": "WMTS",
                "URL": "https:\/\/imagery.wyndham.vic.gov.au\/erdas-iws\/ogc\/wmts\/Aerial?service=WMTS&request=getcapabilities",
                "Leaflet_URL": "https:\/\/imagery.wyndham.vic.gov.au\/erdas-iws\/ogc\/wms\/Aerial?service=WMTS&request=getcapabilities",
                "iOS_URL": "https:\/\/imagery.wyndham.vic.gov.au\/erdas-iws\/ogc\/wmts\/Aerial?service=WMTS&TileMatrixSet=googlemapscompatibleext2:epsg:3857&TileMatrix=\\()&VERSION=1.0.0&style=default&LAYER=2009-11-01&request=GetTile&FORMAT=image\/jpeg&TileRow=\\()&TileCol=\\()",
                "Layer": "2009-11-01",
                "Latest": 0
            },
            "14": {
                "Title": "2007-10",
                "Type": "WMTS",
                "URL": "https:\/\/imagery.wyndham.vic.gov.au\/erdas-iws\/ogc\/wmts\/Aerial?service=WMTS&request=getcapabilities",
                "Leaflet_URL": "https:\/\/imagery.wyndham.vic.gov.au\/erdas-iws\/ogc\/wms\/Aerial?service=WMTS&request=getcapabilities",
                "iOS_URL": "https:\/\/imagery.wyndham.vic.gov.au\/erdas-iws\/ogc\/wmts\/Aerial?service=WMTS&TileMatrixSet=googlemapscompatibleext2:epsg:3857&TileMatrix=\\()&VERSION=1.0.0&style=default&LAYER=2007-10-20&request=GetTile&FORMAT=image\/jpeg&TileRow=\\()&TileCol=\\()",
                "Layer": "2007-10-20",
                "Latest": 0
            },
            "15": {
                "Title": "2006-01",
                "Type": "WMTS",
                "URL": "https:\/\/imagery.wyndham.vic.gov.au\/erdas-iws\/ogc\/wmts\/Aerial?service=WMTS&request=getcapabilities",
                "Leaflet_URL": "https:\/\/imagery.wyndham.vic.gov.au\/erdas-iws\/ogc\/wms\/Aerial?service=WMTS&request=getcapabilities",
                "iOS_URL": "https:\/\/imagery.wyndham.vic.gov.au\/erdas-iws\/ogc\/wmts\/Aerial?service=WMTS&TileMatrixSet=googlemapscompatibleext2:epsg:3857&TileMatrix=\\()&VERSION=1.0.0&style=default&LAYER=2006-01-24&request=GetTile&FORMAT=image\/jpeg&TileRow=\\()&TileCol=\\()",
                "Layer": "2006-01-24",
                "Latest": 0
            },
            "16": {
                "Title": "2005-12",
                "Type": "WMTS",
                "URL": "https:\/\/imagery.wyndham.vic.gov.au\/erdas-iws\/ogc\/wmts\/Aerial?service=WMTS&request=getcapabilities",
                "Leaflet_URL": "https:\/\/imagery.wyndham.vic.gov.au\/erdas-iws\/ogc\/wms\/Aerial?service=WMTS&request=getcapabilities",
                "iOS_URL": "https:\/\/imagery.wyndham.vic.gov.au\/erdas-iws\/ogc\/wmts\/Aerial?service=WMTS&TileMatrixSet=googlemapscompatibleext2:epsg:3857&TileMatrix=\\()&VERSION=1.0.0&style=default&LAYER=2005-12-11&request=GetTile&FORMAT=image\/jpeg&TileRow=\\()&TileCol=\\()",
                "Layer": "2005-12-11",
                "Latest": 0
            }
            */
        };
        
        var baseLayers = {};
        var imgkey;
        var i = 1;
        
        /* there are different imagery layers taken during the years. these are included in imageryobj */
        for (var key in imageryobj) {
            if (imageryobj.hasOwnProperty(key)) { 
                //check tile type to update image variable, there are duplicated variable because it causes issues having the same layer selected between the maps.
                if (imageryobj[key].Type == 'TS') {
                    var image = new L.tileLayer(imageryobj[key].Leaflet_URL,{
                        layers: imageryobj[key].Layer,
                        minZoom: 1,
                        maxZoom:21,
                    });
                } else {
                    var image = new L.tileLayer.wms(imageryobj[key].Leaflet_URL,{
                        layers: imageryobj[key].Layer,
                        minZoom: 1,
                        maxZoom:21,
                    });
                }
                if (i==1) {imgkey=imageryobj[key].Title;}
                baseLayers[imageryobj[key].Title] = image;
            }
            i=i+1;
        }

        var base_lyr = new L.tileLayer('https://imagery.wyndham.vic.gov.au/tiles/base/{z}/{x}/{y}.png',{
            maxNativeZoom:20,
            maxZoom: 21,
            minZoom: 11,
        });
        
        baseLayers['Terrain'] = base_lyr;
        
        var label_lyr = new L.tileLayer('https://imagery.wyndham.vic.gov.au/tiles/road_labels/{z}/{x}/{y}.png',{
            maxNativeZoom:20,
            maxZoom: 21,
            minZoom: 11,
        });
        
        var property_lyr = new L.tileLayer('https://imagery.wyndham.vic.gov.au/tiles/property_aerial/{z}/{x}/{y}.png',{
            maxNativeZoom:20,
            maxZoom: 21,
            minZoom: 16,
        });
        
        var overlays = {
            "Labels": label_lyr,
            "Property": property_lyr,
        };
		
        // create map object
        var map = L.map('map',  {
            center:[-37.877699, 144.681702],
            zoom:14,
            minZoom:11,
            maxZoom:19,
            layers: [baseLayers['Terrain'], label_lyr, property_lyr]
        });

        var dummyMarker = L.marker([0,0]);
		this.uncheckedGroup.addLayer(dummyMarker).addTo(map);
		this.unsuitableGroup.addLayer(dummyMarker).addTo(map);
		this.readyGroup.addLayer(dummyMarker).addTo(map);
		this.plantedGroup.addLayer(dummyMarker).addTo(map);
		
        L.control.scale({position:'bottomleft', maxWidth:100, metric:true, imperial:false}).addTo(map);

        var control = L.control.layers(baseLayers,overlays);
        control.addTo(map);
        
        L.control.createMarkerButton({position:'topright'}).addTo(map);
		
		this.searchCtrl = new fuseSearch(map);
        this.map = map;
        
        //We have to define the crosshair outside of the UI utilities because there's no option for an element to be centred within the map
        var crosshair = document.createElement('div');
		$(crosshair).css({
			'filter': 'invert',
			'position': 'absolute',
			'width': '18px',
			'height': '18px',
			'top': '50%',
			'left': '50%',
			'transform': 'translate(-10px, -10px)',
			'background-color': 'none',
			'border-radius': '10px',
			'border': '1px solid #fff',
			'z-index': 9997,
			'pointer-events': 'none',
			'background':'#018ABD',
			'box-shadow': '0px 0px 5px 5px rgba(2, 138, 189, 0.25)'
		});
		$("#map").append(crosshair);
		
		var markerLayers = {
			"<img src=\"https://cdn.rawgit.com/pointhi/leaflet-color-markers/master/img/marker-icon-blue.png\" /> Unchecked (<span id=\"count_unchecked\">0</span>)": this.uncheckedGroup,
			"<img src=\"https://cdn.rawgit.com/pointhi/leaflet-color-markers/master/img/marker-icon-red.png\" /> Unsuitable (<span id=\"count_unsuitable\">0</span>)": this.unsuitableGroup,
			"<img src=\"https://cdn.rawgit.com/pointhi/leaflet-color-markers/master/img/marker-icon-orange.png\" /> Suitable (<span id=\"count_ready\">0</span>)": this.readyGroup,
			"<img src=\"https://cdn.rawgit.com/pointhi/leaflet-color-markers/master/img/marker-icon-green.png\" /> Planted (<span id=\"count_planted\">0</span>)": this.plantedGroup
		}
		
		L.control.layers(null, markerLayers).addTo(map);
    }; 
	
	getBounds() {
		return this.map.getBounds();
	}
	
	addMarkers(json) {
		var self = this;
		
		var marker = {
			planted: L.icon({
				iconUrl: 'https://cdn.rawgit.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-green.png',
				shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
				iconSize: [25, 41],
				iconAnchor: [12, 41],
				popupAnchor: [1, -34],
				shadowSize: [41, 41]
			}),
			ready: L.icon({
				iconUrl: 'https://cdn.rawgit.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-orange.png',
				shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
				iconSize: [25, 41],
				iconAnchor: [12, 41],
				popupAnchor: [1, -34],
				shadowSize: [41, 41]
			}),
			unsuitable: L.icon({
				iconUrl: 'https://cdn.rawgit.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-red.png',
				shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
				iconSize: [25, 41],
				iconAnchor: [12, 41],
				popupAnchor: [1, -34],
				shadowSize: [41, 41]
			}),
			unchecked: L.icon({
				iconUrl: 'https://cdn.rawgit.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-blue.png',
				shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
				iconSize: [25, 41],
				iconAnchor: [12, 41],
				popupAnchor: [1, -34],
				shadowSize: [41, 41]
			})
		}
		
		if(this.markerLayer){
			this.markerLayer.clearLayers();
			self.plantedGroup.clearLayers();
			self.readyGroup.clearLayers();
			self.unsuitableGroup.clearLayers();
			self.uncheckedGroup.clearLayers();
		}
		
		self.markerLayer = L.geoJSON(json, {
			pointToLayer: function (feature, latlng){
				// Initialise our variables here
				let icn;
				let grp;
				
				// Determine what the status of the
				// plant is and assign it
				switch(feature.properties.status){
					case 'planted':
						icn = marker.planted;
						grp = self.plantedGroup;
						break;
						
					case 'ready':
						icn = marker.ready;
						grp = self.readyGroup;
						break;
						
					case 'unsuitable':
						icn = marker.unsuitable;
						grp = self.unsuitableGroup;
						break;
						
					case 'unchecked':
						icn = marker.unchecked;
						grp = self.uncheckedGroup;
						break;
						
					default:
						icn = marker.unchecked;
						grp = self.uncheckedGroup;
				}
				
				// Here we are creating the 'View Planting Information'
				// button, represented by a glyph of a tree.
				//
				// Create the anchor element
				let plantingButton = document.createElement("a");
				// We don't need a URL, but we need to make it clickable.
				plantingButton.href = "#";
				// We need to use innerHTML here in order for the glyph
				// to render.
				plantingButton.innerHTML = "<i class='huge tree icon'></i>";
				// Bind our click event to the function which displays
				// the form
				$(plantingButton).on("click", function(){
					self.showForm(feature.properties, latlng, this);
				});

				// As above but for comments, represented by a speech
				// bubble
				let commentButton = document.createElement("a");
				commentButton.href = "#";
				commentButton.innerHTML = "<i class='huge talk icon'></i>";
				$(commentButton).on("click", function(){
					self.showAuditComments(feature.properties.id);
				});

				// As above, but for the Audit form, represented by a
				// pen and paper glyph
				let auditButton = document.createElement("a");
				if(feature.properties.status == "planted"){
					auditButton.href = "#";
					auditButton.innerHTML = "<i class='huge write square icon'></i>";
					$(auditButton).on("click", function(){
						self.showPlantingAudit(feature.properties.id);
					});
				}

				// Create the markup for the popup and append our
				// buttons
				let markerPopupMarkup = document.createElement("div");
				markerPopupMarkup.append(plantingButton);
				markerPopupMarkup.append(commentButton);
				markerPopupMarkup.append(auditButton);

				// This function needs to return a success state,
				// which is produced by actually adding the marker
				// to the map.
				return grp.addLayer(
					// Adds a marker to the specified coords with the
					// following properties:
					// icn - the URL we determined above to be the
					//       correct marker image
					// draggable - The marker can be moved, which is
					//             an alternative to marking as
					//             unsuitable and creating a new one
					//			   instead. Either option works
					// properties - The details of the marker retrieved
					//              from the database.
					L.marker(latlng, {
						icon: icn,
						draggable: true,
						properties: feature.properties
					}).on('click', function(e){
						// We no longer display the form directly,
						// instead we show a popup first.
						//self.showForm(this.options.properties, this.getLatLng(), this);
					}).on('moveend', function(e){
						// We do however show the popup directly if the
						// marker was moved.
						var data = this.options.properties.id + ",";
						data += this.getLatLng().lat + ",";
						data += this.getLatLng().lng;
						$.get(
							'https://digital.wyndham.vic.gov.au/treeplanting/updateMarkerPosition/' + data,
						);
						self.showForm(this.options.properties, this.getLatLng(), this, ['comments']);
					}).on('mousedown', function(e){
						// First attempt at hold-to-drag was not successful
						/*var _self = this;
						console.log("predrag");
						_self.dragging.disable();
						var holdTime = 500;
						setTimeout(function(){
							console.log("draggable");
							_self.dragging.enable();
						}, holdTime);*/
					}).bindPopup(markerPopupMarkup)
				);
			}
		});
		
		var grp = [
			this.plantedGroup,
			this.readyGroup,
			this.unsuitableGroup,
			this.uncheckedGroup
		]
		
		$.each(grp, function(key, value){
			if(self.map.hasLayer(value)){
				value.addTo(this.map);
			}
		});
	}
	
	showForm(properties, latlng, marker, mandatoryFields = []){
		var self = this;
		let readOnly = false;

		$.getJSON('https://digital.wyndham.vic.gov.au/treeplanting/getMarkerMetadata/' + properties.id, function(properties){
			//Marker definitions object
			var markers = {
				planted: L.icon({
					iconUrl: 'https://cdn.rawgit.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-green.png',
					shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
					iconSize: [25, 41],
					iconAnchor: [12, 41],
					popupAnchor: [1, -34],
					shadowSize: [41, 41]
				}),
				ready: L.icon({
					iconUrl: 'https://cdn.rawgit.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-orange.png',
					shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
					iconSize: [25, 41],
					iconAnchor: [12, 41],
					popupAnchor: [1, -34],
					shadowSize: [41, 41]
				}),
				unsuitable: L.icon({
					iconUrl: 'https://cdn.rawgit.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-red.png',
					shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
					iconSize: [25, 41],
					iconAnchor: [12, 41],
					popupAnchor: [1, -34],
					shadowSize: [41, 41]
				}),
				unchecked: L.icon({
					iconUrl: 'https://cdn.rawgit.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-blue.png',
					shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
					iconSize: [25, 41],
					iconAnchor: [12, 41],
					popupAnchor: [1, -34],
					shadowSize: [41, 41]
				})
			}
			
			//The blanking background
			var popupBg = document.createElement("div");
			$(popupBg).addClass("popupBg");
			$(popupBg).on("click", function(){
				if(mandatoryFields.length < 1)
					$(this).remove();
			});
			
			//The Form
			var formDiv = document.createElement("div");
			$(formDiv).addClass("popupForm");
			$(formDiv).on("click", function(e){
				e.stopPropagation();
			});
			
			if(properties.status == "planted"){
				readOnly = true;
				var inspectButton = document.createElement("button");
				inspectButton.type = "button";
				inspectButton.innerHTML = "Inspect";
				inspectButton.className = "inspectButton";
				formDiv.appendChild(inspectButton);

				$(inspectButton).on("click", function(){
					$(popupBg).remove();
					self.showPlantingAudit(properties.id);
				});

				var commentsButton = document.createElement("button");
				commentsButton.type = "button";
				commentsButton.innerHTML = "Comments";
				commentsButton.className = "commentsButton";
				formDiv.appendChild(commentsButton);

				$(commentsButton).on("click", function(){
					$(popupBg).remove();
					self.showAuditComments(properties.id);
				});
			}
			//Heading
			var heading = document.createElement("h3");
			heading.innerHTML = "Tree Information";
			
			//The beginnings of the form
			var form = document.createElement("div");
			$(form).addClass("form");
			
			//ID
			var idHidden = document.createElement("input");
			idHidden.type = "hidden";
			idHidden.name = "id";
			idHidden.id = "id";
			idHidden.value = properties.id;
			form.appendChild(idHidden);
			
			//Address
			form.innerHTML += "Nearest Address:";
			var addressInput = document.createElement("input");
			addressInput.name = "address";
			addressInput.id = "address";
			$(addressInput).attr("value", "Locating... <i class='fa fa-spin fa-refresh' aria-hidden='true'></i>");
			form.appendChild(addressInput);

			if(properties.nearest_address){
				$(addressInput).attr("value", properties.nearest_address);
			} else {
				$.get("https://digital.wyndham.vic.gov.au/treeplanting/getNearestAddress/" + latlng.lat + "|" + latlng.lng, function(resp){
					$("#address").attr("value", resp);
				});
			}
			
			if(readOnly)
				addressInput.disabled = true;

			//Loading placeholder
			var loadingPlaceholder = document.createElement("option");
			loadingPlaceholder.innerHTML = "Loading...";
			
			// Create the tree species form

			// Botanical Name
			form.innerHTML += "Botanical Name:"
			var botanicalHidden = document.createElement("input");
			botanicalHidden.type = "hidden";
			botanicalHidden.name = "ex_botanical_name";
			botanicalHidden.id = "ex_botanical_name";
			botanicalHidden.value = properties.botanical_name;
			form.appendChild(botanicalHidden);
			
			var botanicalInput = document.createElement("select");
			botanicalInput.name = "botanical_name";
			botanicalInput.id = "botanical_name";
			botanicalInput.appendChild(loadingPlaceholder);
			form.appendChild(botanicalInput);
			
			if(readOnly)
				botanicalInput.disabled = true;

			// Common Name
			form.innerHTML += "Common Name:"
			var commonHidden = document.createElement("input");
			commonHidden.type = "hidden";
			commonHidden.name = "ex_common_name";
			commonHidden.id = "ex_common_name";
			commonHidden.value = properties.common_name;
			form.appendChild(commonHidden);
			
			var commonInput = document.createElement("select");
			commonInput.name = "common_name";
			commonInput.id = "common_name";
			commonInput.appendChild(loadingPlaceholder);
			form.appendChild(commonInput);

			if(readOnly)
				commonInput.disabled = true;

			// Get the array of tree names
			$.get('https://digital.wyndham.vic.gov.au/treeplanting/getTreeNameArray', function(data){ 
				data = JSON.parse(data);

				$("#botanical_name").html("");
				$("#common_name").html("");

				// Populate the select lists
				data.forEach(option => {
					let newBotanicalOption = document.createElement("option");
					let newCommonOption = document.createElement("option");

					newBotanicalOption.innerHTML = option.tree_botanical;
					newCommonOption.innerHTML = option.tree_common;

					newBotanicalOption.value = option.tree_type_id;
					newCommonOption.value = option.tree_type_id;

					$("#botanical_name").append(newBotanicalOption);
					$("#common_name").append(newCommonOption);
				});

				$(botanicalInput).select2({
					dropdownParent: $('.popupForm')
				});

				$(commonInput).select2({
					dropdownParent: $('.popupForm')
				});
				
				// Find correct tree item id and fill
				let treeTypeID = -1;

				data.forEach(option => {
					if(option.tree_botanical == properties.botanical_name){
						treeTypeID = option.tree_type_id;
						$("#botanical_name").val(treeTypeID);
						$("#common_name").val(treeTypeID);
						return false;
					} else if (option.tree_common == properties.common_name) {
						treeTypeID = option.tree_type_id;
						$("#botanical_name").val(treeTypeID);
						$("#common_name").val(treeTypeID);
						return false;
					}
				});

				// We didn't find a matching tree, so now we perform a fuzzy search for
				// entries which may be the tree name
				if(treeTypeID < 0){
					let fuzzy_botanical = properties.botanical_name.substr(0,5);
					let fuzzy_common = properties.common_name.substr(0,5);
					$.get('https://digital.wyndham.vic.gov.au/treeplanting/fuzzyTreeNameSearch/' + fuzzy_botanical + "|" + fuzzy_common, function(data){
						if(data[0]){
							treeTypeID = data[0].tree_type_id;
							$("#botanical_name").val(treeTypeID);
							$("#common_name").val(treeTypeID);
						} else {
							alert("Could not match tree species \"" + properties.botanical_name + "\" to any plants in database. Please select the correct tree species from dropdown list.");
						}
					})
				}
			});

			//New Comments
			form.innerHTML += "Comments Thread:"
			var commentsDiv = document.createElement("div");
			commentsDiv.id = "comments_display";
			commentsDiv.innerHTML = "Loading...";
			form.appendChild(commentsDiv);
			
			$.get("https://digital.wyndham.vic.gov.au/treeplanting/getTreeComments/" + properties.id, function(data){
				$("#comments_display").html("");
				try {
					//Legacy Comments
					if(properties.comments){
						var comment = document.createElement("div");
						var commentLayout = "<p class='comment_message'>" + properties.comments + "</p>\
											 <small class='comment_footer'>- Legacy Comment</small>";
						comment.innerHTML = commentLayout;
						comment.className = "comment";
						$("#comments_display").append(comment);
					}
					//End Legacy Comments
					if(data.length > 0){
						$.each(data, function (key, value){
							$.get("https://digital.wyndham.vic.gov.au/treeplanting/userFromID/" + value.user_id, function(data){
								
								var username = data[0].name;
								
								var comment = document.createElement("div");
								
								var clearfix;
								
								//Handling the date/time
								var dateTime = new Date(value.timestamp.replace(/-/g,"/"));
								var currTime = new Date();
								
								var timestamp;
								
								if(dateTime > (currTime - 86400000)){
									timestamp = " at " + dateTime.getHours() + ":" + dateTime.getMinutes();
								} else if(dateTime > (currTime - 604800000)){
									var days = ['Sun', 'Mon', 'Tues', 'Wed', 'Thurs', 'Fri', 'Sat'];
									timestamp = " on " + days[dateTime.getDay()];
								} else {
									timestamp = " on " + dateTime.getDate() + "/" + dateTime.getMonth();
								}
								
								//The layout for each comment - styles defined in CSS
								
								var commentLayout = "<p class='comment_message'>" + value.message + "</p>\
													 <small class='comment_footer'>- " + username + timestamp + "</small>";
								
								//End comment layout
								
								comment.innerHTML = commentLayout;
								
								if(gUser == username){
									comment.className = "my_comment";
									clearfix = document.createElement("div");
									clearfix.className = "clearfix";
									
								} else 
									comment.className = "comment";
									
								$("#comments_display").append(comment);
							});
						});
					} else if($(".comment").length < 1){
						var comment = document.createElement("div");
						var commentLayout = "<p class='comment_message'>There are no comments on this specimen yet</p>\
											 <small class='comment_footer'>- Admin</small>";
						comment.innerHTML = commentLayout;
						comment.className = "comment";
						$("#comments_display").append(comment);
					}
				}
				catch (err){
					$("#comments_display").append("Could not retrieve messages from server. Please try again.");
				}
			});
			
			var commentInput = document.createElement("input");
			commentInput.id = "add_comment_input";
			var commentSubmit = document.createElement("button");
			commentSubmit.textContent = "Add";
			commentSubmit.id = "add_comment";
			
			if(readOnly){
				commentInput.disabled = true;
				commentSubmit.disabled = true;
			}

			$(form).on('click', '#add_comment', function(){
				$.get("https://digital.wyndham.vic.gov.au/treeplanting/addTreeComment/" + properties.id + "|" + $("#add_comment_input").val(), function(){
					var comment = document.createElement("div");
					comment.className = "my_comment";
					var username = gUser;
					
					//The layout for each comment - styles defined in CSS
					
					var commentLayout = "<p class='comment_message'>" + $("#add_comment_input").val() + "</p>\
										 <small class='comment_footer'>" + username + " - Just Now</small><div style='clear:both'></div>";
					
					//End comment layout
					
					$("#add_comment_input").val("");
					
					comment.innerHTML = commentLayout;
					$("#comments_display").append(comment);
					$("#comments_display").animate({ scrollTop: 9999 }, 1000);
				});
			});
			
			form.appendChild(commentInput);
			form.appendChild(commentSubmit);
			
			//Status Selector
			form.innerHTML += "Status:";
			var statusSelectorHidden = document.createElement("input");
			statusSelectorHidden.type = "hidden";
			statusSelectorHidden.name = "status";
			statusSelectorHidden.id = "status";
			if(properties.status){
				statusSelectorHidden.value = properties.status;
			} else {
				statusSelectorHidden.value = "unchecked";
			}
			form.appendChild(statusSelectorHidden);
			
			var statusSelectorDiv = document.createElement("div");
			statusSelectorDiv.className = "status-radio";
			
			var buttons = [
				{
					"id": "unchecked",
					"symbol": "fa fa-question-circle fa-4x"
				},
				{
					"id": "unsuitable",
					"symbol": "fa fa-ban fa-4x"
				},
				{
					"id": "ready",
					"symbol": "fa fa-check fa-4x"
				},
				{
					"id": "planted",
					"symbol": "fa fa-tree fa-4x"
				}
			];
			
			$.each(buttons, function(key, value){
				var radioButtonDiv = document.createElement("div");
				if(statusSelectorHidden.value == value.id){
					radioButtonDiv.className = "status-radio-button active";
				} else {
					radioButtonDiv.className = "status-radio-button";	
				}
				radioButtonDiv.id = value.id;
				
				var radioButtonSymbol = document.createElement("i");
				radioButtonSymbol.className = value.symbol;
				
				radioButtonDiv.appendChild(radioButtonSymbol);
				statusSelectorDiv.appendChild(radioButtonDiv);
			});
			
			form.appendChild(statusSelectorDiv);
			
			if(properties.status_changed != null){
				form.innerHTML += "<br /><small>Last updated on " + properties.status_changed + " by " + properties.status_changed_user + "</small>";
			} else {
				form.innerHTML += "<br /><small>Last updated never by nobody</small>";
			}
			
			var submitButton = document.createElement("button");
			submitButton.innerHTML = "Save";
			
			if(readOnly)
				submitButton.disabled = true;

			$(submitButton).on('click', function(){
				var id = idHidden.value;
				var data = new Object;
				
				data['id'] = id;
				data['previous_botanical_name'] = properties.botanical_name;
				data['botanical_name'] = $("#botanical_name").val();
				data['previous_common_name'] = properties.common_name;
				data['common_name'] = $("#common_name").val();
				data['comments'] = $("#comments").val();
				data['address'] = $("#address").val();
				
				var reqString = "";
				$.each(data, function(k, v){
					reqString += v + "|";
				});
				
				if(!mandatoryFields['comments'] && $(statusSelectorHidden).attr("value") == "unsuitable"){
					mandatoryFields.push("comments");
				}
				
				var canSave;

				if(mandatoryFields[0] == "comments"){
					if($('.my_comment').length > 0){
						canSave = true;
					} else {
						canSave = false;
					}
				} else {
					canSave = true;
				}
				
				if(canSave){
					$(this).html("Saving...");
					$.get(
						'https://digital.wyndham.vic.gov.au/treeplanting/updateMarkerMetadata/' + reqString,
						function(resp){
							$(this).html("Saved!");
							$(popupBg).remove();
						}
					);
				} else {
					alert("Please add a comment indicating the reason behind this change");
				}
			});
			
			form.appendChild(submitButton);
			
			$(formDiv).append(heading);
			$(formDiv).append(form);
			
			$("body").append(popupBg);
			$(popupBg).append(formDiv);
			
			$(".status-radio-button").on('click', function(){
				$(".status-radio-button").removeClass('active');
				$(this).addClass('active');
				$("#status").val($(this).prop("id"));
				//gUser is defined in the blade - there should be a cleaner and more secure way to do this
				$.get("https://digital.wyndham.vic.gov.au/treeplanting/updateTreeStatus/" + properties.id + "|" + $("#status").val() + "|" + gUser);
				switch($("#status").val()){
					case 'unchecked':
						marker.setIcon(markers.unchecked);
						break;
						
					case 'unsuitable':
						marker.setIcon(markers.unsuitable);
						break;
						
					case 'ready':
						marker.setIcon(markers.ready);
						break;
						
					case 'planted':
						marker.setIcon(markers.planted);
						break;
						
					default:
						marker.setIcon(markers.unchecked);
				}
			});
			
			$("#common_name, #botanical_name").on("change", function(){
				var value = $(this).val();
				$("#common_name").val(value);
				$('#common_name').trigger('change.select2');
				$("#botanical_name").val(value);
				$('#botanical_name').trigger('change.select2');
			});
		});
	}

	showPlantingAudit(treeID){
		var _self = this;
		let auditDisabled = true

		if(gRole != "Contractor"){
			auditDisabled = false;
		}

		//List of fields to use in Planting Audit form
		let auditFields = [
			{
				name : 'is_healthy',
				label: 'Tree is healthy'
			},
			{
				name : 'is_strong',
				label: 'Tree has strong central leader (species dependant)'
			},
			{
				name : 'hole_size',
				label: 'Size of hole is in accordance with specifications'
			},
			{
				name : 'gypsum_applied',
				label: '500g of Gypsum has been applied'
			},
			{
				name : 'root_ball_pruned',
				label: 'Root ball has been pruned into quadrants (where required)'
			},
			{
				name : 'hole_backfilled',
				label: 'Hole has been backfilled with 50/50 soil blend'
			},
			{
				name : 'waterwell_placed',
				label: 'Waterwell is in place, Level to 150mm above ground'
			},
			{
				name : 'is_mulched',
				label: 'Sufficient mulch has been applied to base of tree'
			},
			{
				name : 'stakes_placed',
				label: 'Stakes are in place with ties loose enough to allow tree movement'
			},
			{
				name : 'tree_watered',
				label: 'Tree has been watered with root promoting hormone'
			}
		];

		// Constructing the form
		let form = document.createElement('form');
		form.className = 'form';
		//// Heading
		let heading = document.createElement('h3');
		heading.innerText = "Tree Planting Audit";
		form.append(heading);

		if(auditDisabled){
			let disclaimer = document.createElement("p");
			disclaimer.innerText = "This form is read-only."
			form.append(disclaimer);
		}
		//// Fields
		let treeidHidden = document.createElement('input');
		treeidHidden.type = 'hidden';
		treeidHidden.name = 'tree_id';
		treeidHidden.value = treeID;

		form.append(treeidHidden);

		let useridHidden = document.createElement('input');
		useridHidden.type = 'hidden';
		useridHidden.name = 'operator_id';
		useridHidden.value = gUserID;

		form.append(useridHidden);

		$.each(auditFields, function(key, field){
			let rowContainer = document.createElement('div');
			rowContainer.className = 'auditFieldContainer';
			let leftSideDiv = document.createElement('div');
			leftSideDiv.className = 'leftSideDiv';
			let rightSideDiv = document.createElement('div');
			rightSideDiv.className = 'rightSideDiv';
			let checkbox = document.createElement('input');
			checkbox.disabled = auditDisabled;

			let label = document.createElement('label');
			$(label).html(field.label);
			$(label).attr('for', field.name);

			leftSideDiv.appendChild(label);

			$(checkbox).attr('type', 'checkbox');
			$(checkbox).attr('name', field.name);
			$(checkbox).attr('id', field.name);
			$(checkbox).attr('value', 'true');
			$(checkbox).addClass('auditCheckbox');

			rightSideDiv.appendChild(checkbox);

			$(rowContainer).append(leftSideDiv);
			$(rowContainer).append(rightSideDiv);
			$(form).append(rowContainer);
		});

		var elems = Array.prototype.slice.call(document.querySelectorAll('.auditCheckbox'));
		elems.forEach(function (html) {
			var switchery = new Switchery(html);
		});

		//// Picture Upload
		let pictureContainer = document.createElement('div');
		pictureContainer.className = "pictureContainer";

		let imageContainer = document.createElement('div');
		imageContainer.className = 'imageContainer';

		let uploadElem = document.createElement('input');
		uploadElem.type = 'file';
		uploadElem.disabled = auditDisabled;
		uploadElem.className = 'auditFormFileSubmit';
		uploadElem.id = 'auditFormFileSubmit';
		uploadElem.accept = 'image/*;capture=camera';

		let uploadElemLabel = document.createElement('label');
		uploadElemLabel.htmlFor = 'auditFormFileSubmit';
		uploadElemLabel.innerHTML = '<i class="fa fa-4x fa-plus" aria-hidden="true"></i>';

		let azureContainer = "tree-" + treeID;
		let azureURI = _self.azureURI;

		$(uploadElem).on('change', function(){
			let file = uploadElem.files[0];
			let fr = new FileReader();

			fr.onload = function(e){
				let image = e.target.result;

				// Get SAS from server
				$.get('/generateSAS', function(SAS){
					let containerRequest = new XMLHttpRequest();

					// Create a filename for the image - it can be anything, so long as it's unique
					let file = "/" + Date.now() + ".jpg";

					// Create a new container if it doesn't already exist
					containerRequest.open("PUT", azureURI + azureContainer + "?restype=container&" + SAS);
					containerRequest.setRequestHeader('x-ms-blob-public-access', 'container');
					containerRequest.setRequestHeader('x-ms-blob-type', 'BlockBlob');
					containerRequest.send('');

					let uploadRequest = new XMLHttpRequest();
					uploadRequest.open("PUT", azureURI + azureContainer + file + "?" + SAS, true);

					uploadRequest.onload = function(e) {
						let imgRequest = new XMLHttpRequest();
						imgRequest.open("GET", azureURI + azureContainer + file + "?" + SAS, true);
						imgRequest.overrideMimeType("image.jpeg; charset=x-user-defined");
						imgRequest.onloadend = function(resp){
							let uploadedImage = document.createElement('img');
							uploadedImage.className = 'treeAuditImage';
							uploadedImage.src = 'data:image/jpeg;base64, ' + btoa(resp.target.response);
							$(imageContainer).append(uploadedImage);
						}
						imgRequest.send();
					}

					uploadRequest.setRequestHeader('x-ms-blob-type', 'BlockBlob');
					uploadRequest.setRequestHeader('x-ms-blob-content-type', file.type);
					uploadRequest.setRequestHeader('x-ms-meta-uploadvia', 'Tree Planting Web Application');
					uploadRequest.setRequestHeader('x-ms-meta-user', gUser);
					uploadRequest.overrideMimeType('multipart/form-data; charset=x-user-defined-binary');

					uploadRequest.send(image);
				});
			};
			fr.readAsBinaryString(file);
		});

		$(imageContainer).append(uploadElem);
		$(imageContainer).append(uploadElemLabel);

		$.get({
			url: this.azureURI + azureContainer + "?restype=container&comp=list&include=metadata",
			success: function(data){
				$.get('/generateSAS', function(SAS){
					let images = data.evaluate('EnumerationResults/Blobs/Blob/Name', data, null, XPathResult.UNORDERED_NODE_ITERATOR_TYPE, null);
					try {
						var image = images.iterateNext();
						while(image){
							let imgRequest = new XMLHttpRequest();
							imgRequest.open("GET", azureURI + azureContainer + "/" + $(image).html() + "?" + SAS, true);
							imgRequest.overrideMimeType("image.jpeg; charset=x-user-defined");
							imgRequest.onloadend = function(resp){
								let uploadedImage = document.createElement('img');
								uploadedImage.className = 'treeAuditImage';
								uploadedImage.src = 'data:image/jpeg;base64, ' + btoa(resp.target.response);
								$(imageContainer).append(uploadedImage);
							}
							imgRequest.send();
							image = images.iterateNext();
						}
					} catch(e){
						
					}
				})
			},
			dataType: 'XML'
		});

		
		/*$('.treeAuditImage').on('click', function(){
			let file_id = $(this).data('id');
			$.get("https://digital.wyndham.vic.gov.au/treeplanting/getPlantingAuditPhoto/" + file_id, function(data){
				// Create window to pop up image
				// Display data
			});
		});*/

		$(form).append(imageContainer);

		//// Submit Button
		let submitButton = document.createElement('button');
		submitButton.type = 'button';
		submitButton.innerText = 'Save';
		submitButton.disabled = auditDisabled;

		$(submitButton).on("click", function(){
			console.log($(".form").serialize());
			$.get('/api/createAudit/' + $(".form").serialize(), function(data){
				console.log(data);
			});
		});

		form.appendChild(submitButton);

		// Boilerplate for the form
		var popupBg = document.createElement("div");
		$(popupBg).addClass("popupBg");
		$(popupBg).on("click", function(){
			$(this).remove();
		});
		
		var formDiv = document.createElement("div");
		$(formDiv).addClass("popupForm");
		$(formDiv).on("click", function(e){
			e.stopPropagation();
		});

		// Combine form with boilerplate
		$(formDiv).append(form);
		$(popupBg).append(formDiv);

		// Display!
		$('body').append(popupBg);
	}

	showAuditComments(treeID){
		// Constructing the form
		let form = document.createElement('div');
		form.className = 'form';
		//// Heading
		let heading = document.createElement('h3');
		heading.innerText = "Comments";
		form.appendChild(heading);

		let commentsDisplay = document.createElement('div');
		commentsDisplay.className = 'commentsFormDisplay';
		
		$(commentsDisplay).html('<div class="comment_loading_msg">Loading... <i class="fa fa-spin fa-circle-o-notch" aria-hidden="true"></i></div>');

		$.get("https://digital.wyndham.vic.gov.au/treeplanting/getTreeCommentsRendered/" + treeID, function(data){
			$(commentsDisplay).html(data);
			$(commentsDisplay).animate({ scrollTop: 9999 }, 1000);
		});

		let commentsFormInput = document.createElement('input');
		commentsFormInput.type = 'text';
		
		let commentsFormSubmit = document.createElement('button');
		commentsFormSubmit.type = 'button';
		commentsFormSubmit.innerText = 'Send';

		$(commentsFormSubmit).on("click", function(){
			// Push data to server
			$.get("https://digital.wyndham.vic.gov.au/treeplanting/addTreeComment/" + treeID + "|" + $(commentsFormInput).val(), function(){

				// Append comment to view
				var comment = document.createElement("div");
				comment.className = "my_comment";
				var username = gUser;
				
				//The layout for each comment - styles defined in CSS
				
				var commentLayout = "<p class='comment_message'>" + $(commentsFormInput).val() + "</p>\
										<small class='comment_footer'>" + username + " - Just Now</small><div style='clear:both'></div>";
				
				//End comment layout
				
				$(commentsFormInput).val("");
				
				comment.innerHTML = commentLayout;
				$(commentsDisplay).append(comment);
				$(commentsDisplay).animate({ scrollTop: 9999 }, 1000);
			});
		});

		$(form).append(commentsDisplay);
		$(form).append(commentsFormInput);
		$(form).append(commentsFormSubmit);

		// Boilerplate for the form
		var popupBg = document.createElement("div");
		$(popupBg).addClass("popupBg");
		$(popupBg).on("click", function(){
			$(this).remove();
		});
		
		var formDiv = document.createElement("div");
		$(formDiv).addClass("popupForm");
		$(formDiv).on("click", function(e){
			e.stopPropagation();
		});

		// Combine form with boilerplate
		$(formDiv).append(form);
		$(popupBg).append(formDiv);

		// Display!
		$('body').append(popupBg);
	}

	showAuditReview(treeID){
		
	}
}