function getPoints(map){
	var bounds = [
		map.getBounds().getNorthEast().lat,
		map.getBounds().getSouthWest().lat,
		map.getBounds().getSouthWest().lng,
		map.getBounds().getNorthEast().lng
	]
	var data = "";
	
	$.each(bounds, function(k, v){
		data += v
		if(k < 3){
			data += ",";
		}
	});
	
	$.get('https://digital.wyndham.vic.gov.au/treeplanting/getGeoJson/' + data, null, function(data){
		map.addMarkers(data.features);
	});
}

function getSearchData(map){
	$.get('https://digital.wyndham.vic.gov.au/treeplanting/getSearchData', null, function(data){
		map.searchCtrl.indexFeatures(data, ['property_address']);
		$.each(data.features, function(key, feature){
			switch(feature.properties.status){
				case 'planted':
					$("#count_planted").html(parseInt($("#count_planted").html()) + 1);
					break;
					
				case 'ready':
					$("#count_ready").html(parseInt($("#count_ready").html()) + 1);
					break;
					
				case 'unsuitable':
					$("#count_unsuitable").html(parseInt($("#count_unsuitable").html()) + 1);
					break;
					
				case 'unchecked':
					$("#count_unchecked").html(parseInt($("#count_unchecked").html()) + 1);
					break;
					
				default:
					$("#count_unchecked").html(parseInt($("#count_unchecked").html()) + 1);
			}
		})
	});
}

$(document).ready(function(){
	var wmaps = new WyndhamMaps();
		wmaps.initMap();
		
	getSearchData(wmaps);
	getPoints(wmaps);
	
	wmaps.map.on("moveend", function(){
		getPoints(wmaps);
		wmaps.map.invalidateSize(true);
	});
});