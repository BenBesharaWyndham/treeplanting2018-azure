function validEmpty(point, e, tagName) {
  if(point.val() == '') {
    e.target.setCustomValidity('The '+ tagName +' field is required.');
    var hasErr = point.next().length;
    point.parent().parent().addClass('has-error');
    if(hasErr <= 0)
      point.parent().append('<span class="help-block"><strong>'+ e.target.validationMessage +'</strong></span>');
  } else {
    e.target.setCustomValidity('');
    point.parent().parent().removeClass('has-error');
    point.siblings().remove();
  }
}

function validEmail(point, e) {
  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  if(!regex.test(point.val())) {
    e.target.setCustomValidity('The email must be a valid email address.');
    var hasErr = point.next().length;
    point.parent().parent().addClass('has-error');
    if(hasErr <= 0)
      point.parent().append('<span class="help-block"><strong>'+ e.target.validationMessage +'</strong></span>');
  } else {
    e.target.setCustomValidity('');
    point.parent().parent().removeClass('has-error');
    point.siblings().remove();
  }
}

$(function() {
  $("#email").focusout(function(e) {
    validEmpty($(this), e, 'email');
    validEmail($(this), e);
  });

  $("#password").focusout(function(e) {
    validEmpty($(this), e, 'password');
  });

  $("#password-confirm").focusout(function(e) {
    validEmpty($(this), e, 'confirm password');
  });
});