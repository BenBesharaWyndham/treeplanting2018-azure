var checkedVals = []; //global vals of checked

function delSeleted(vals){

  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    beforeSend:function(){
      // show gif here, eg:
      $("#loading").show();
    },
    complete:function(){
      // hide gif here, eg:
      $("#loading").hide();
    }
  }),

    $.ajax(
      {
        url: '/admin/delete',
        type: "POST",
        dataType: "json",
        data: { ids: vals },
        success: function (data) {

        }
      }
    );
}

function checkall(val) {

  $('#select_all').on('change', function(){
    var table = val.DataTable();
    var cells = table.cells( ).nodes();
    $( cells ).find(':checkbox').prop('checked', $(this).is(':checked'));

    checkedVals = $('input[name=cb]:checked').map(function()
    {
      return $(this).val();
    }).get();
  });
}


$(document).ready(function(){

  $('#fromDate').datepicker({format: 'yyyy-mm-dd'});
  $('#toDate').datepicker({format: 'yyyy-mm-dd'});

//    ajax call for search tree via ID
  $('#searchIDForm').submit(function(e) {
    e.preventDefault();

    var data = $(this).serialize();

    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      beforeSend:function(){
        // show gif here, eg:
        $("#loading").show();
      },
      complete:function(){
        // hide gif here, eg:
        $("#loading").hide();
      }
    }),

      $.ajax(
        {
          url: $(this).attr('action'),
          type: "POST",
          dataType: "json",
          data: data,
          success: function (data) {
            $('#result').empty();
            $('#result').append('<table id="resultByID" class="display" cellspacing="0" width="100%"></table>');
            $('#resultByID').append('<thead><tr><th>Planting ID</th><th>Near Address</th><th>Notify Address</th><th>Latitude</th><th>Longitude</th><th>Locality</th><th>Tree Common</th><th>Tree Botanical</th><th>Tree Origin</th><th>Status</th></tr></thead>');
            $('#resultByID').append('<tbody><tr><td>'+ data.planting_id +'</td>' +
              '<td>'+ data.near_address + '</td>' +
              '<td>'+ data.notify_address+'</td>' +
              '<td>'+ data.latitude +'</td>' +
              '<td>'+ data.longitude +'</td>' +
              '<td>'+ data.locality +'</td>' +
              '<td>'+ data.tree_common +'</td>' +
              '<td>'+ data.tree_botanical +'</td>' +
              '<td>'+ data.tree_origin +'</td>' +
              '<td>'+ data.status +'</td></tr></tbody>');

//              $('#result').append('<button type="button" class="btn btn-danger" id="delItem">Delete</button>');

            $('#resultByID').DataTable({
              dom: 'Bfrtip',
              buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
              ],
//                aoColumns: [
////                  { "bSortable": false },
//                  null,
//                  null,
//                  null,
//                  null,
//                  null,
//                  null,
//                  null
//                ]
            });

            //    check all
//              checkall($('#resultByID'));
//
//              //    check single record
//              $('input[name="cb"]').on('change', function() {
//                if($(this).is(':checked')) {
//                  var item = $(this).val();
//                  if($.inArray(item, checkedVals) == -1) {
//                    checkedVals.push(item);
//                  }
//                } else {
//                  checkedVals.splice($.inArray($(this).val(), checkedVals),1);
//                }
//              });
//
//              $('#delItem').click(function() {
//                delSeleted(checkedVals);
//              });
          },
          error: function(data) {

          }
        }
      )
  });


//    ajax call for search tree via Near Address
  $('#searchAddrForm').submit(function(e) {
    e.preventDefault();

    var data = $(this).serialize();

    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      beforeSend:function(){
        // show gif here, eg:
        $("#loading").show();
      },
      complete:function(){
        // hide gif here, eg:
        $("#loading").hide();
      }
    }),

      $.ajax(
        {
          url: $(this).attr('action'),
          type: "POST",
          dataType: "json",
          data: data,
          success: function (data) {
            $('#result').empty();
            $('#result').append('<table id="resultByAdd" class="display" cellspacing="0" width="100%"></table>');
            $('#resultByAdd').append('<thead><tr><th>Planting ID</th><th>Near Address</th><th>Notify Address</th><th>Latitude</th><th>Longitude</th><th>Locality</th><th>Tree Common</th><th>Tree Botanical</th><th>Tree Origin</th><th>Status</th></tr></thead>');
            $('#resultByAdd').append('<tbody id="itemAdd"></tbody>');

            for(i=0; i<data.length; i++) {
              $('#itemAdd').append('<tr><td>'+ data[i].planting_id + '</td>' +
                '<td>'+ data[i].near_address + '</td>' +
                '<td>'+ data[i].notify_address+'</td>' +
                '<td>'+ data[i].latitude +'</td>' +
                '<td>'+ data[i].longitude +'</td>' +
                '<td>'+ data[i].locality +'</td>' +
                '<td>'+ data[i].tree_common +'</td>' +
                '<td>'+ data[i].tree_botanical +'</td>' +
                '<td>'+ data[i].tree_origin +'</td>' +
                '<td>'+ data[i].status +'</td></tr>');
            }

//              $('#result').append('<button type="button" class="btn btn-danger" id="delItem">Delete</button>');

            $('#resultByAdd').DataTable({
              dom: 'Blfrtip',
              lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
              buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
              ]
//                aoColumns: [
////                  { "bSortable": false },
//                  null,
//                  null,
//                  null,
//                  null,
//                  null,
//                  null,
//                  null
//                ]
            });

            //    check all
//              checkall($('#resultByAdd'));

          },
          error: function(data) {

          }
        }
      )
  });


//    ajax call for search tree via date range
  $('#searchDateTime').submit(function(e) {
    e.preventDefault();

    var data = $(this).serialize();

    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      beforeSend:function(){
        // show gif here, eg:
        $("#loading").show();
      },
      complete:function(){
        // hide gif here, eg:
        $("#loading").hide();
      }
    }),

      $.ajax(
        {
          url: $(this).attr('action'),
          type: "POST",
          dataType: "json",
          data: data,
          success: function (data) {
            $('#result').empty();
            $('#result').append('<table id="resultByDateTime" class="display" cellspacing="0" width="100%"></table>');
            $('#resultByDateTime').append('<thead><tr><th>Planting ID</th><th>Near Address</th><th>Notify Address</th><th>Latitude</th><th>Longitude</th><th>Locality</th><th>Tree Common</th><th>Tree Botanical</th><th>Tree Origin</th><th>Status</th></tr></thead>');
            $('#resultByDateTime').append('<tbody id="itemAdd"></tbody>');

            for(i=0; i<data.length; i++) {
              $('#itemAdd').append('<tr><td>'+ data[i].planting_id +'</td>' +
                '<td>'+ data[i].near_address + '</td>' +
                '<td>'+ data[i].notify_address+'</td>' +
                '<td>'+ data[i].latitude +'</td>' +
                '<td>'+ data[i].longitude +'</td>' +
                '<td>'+ data[i].locality +'</td>' +
                '<td>'+ data[i].tree_common +'</td>' +
                '<td>'+ data[i].tree_botanical +'</td>' +
                '<td>'+ data[i].tree_origin +'</td>' +
                '<td>'+ data[i].status +'</td></tr>');
            }

//                $('#result').append('<button type="button" class="btn btn-danger" id="delItem">Delete</button>');

            $('#resultByDateTime').DataTable({
              dom: 'Blfrtip',
              buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
              ],
              lengthMenu: [ [10, 100, 500, -1], [10, 100, 500, "All"] ]
            });

            //    check all
//                checkall($('#resultByDateTime'));

          },
          error: function(data) {

          }
        }
      )
  });


//    upload csv file via ajax
  $('#myForm').submit(function(e){
    e.preventDefault();
//      check upload file format
    var ext = $('#file').val().split('.').pop().toLowerCase();
    if($.inArray(ext, ['csv']) == -1) { // only allow csv format
      $('#error').hide();
      $('#success').hide();
      $('#error').show();
      return false;
    }

    var form = document.forms.namedItem("myForm");
    var data = new FormData(form);

    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      beforeSend:function(){
        // show gif here, eg:
        $("#loading").show();
      },
      complete:function(){
        // hide gif here, eg:
        $("#loading").hide();
      }
    }),

      $.ajax(
        {
          async: true,
          url: $(this).attr('action'),
          type: "POST",
          dataType: "json",
          contentType: false,
          data: data,
          processData: false,
          success: function (data) {
            $('#error').hide();
            $('#success').hide();
            $('#success').show();
          },
          error: function(data) {

          }
        }
      )
  });
});