class fuseSearch {
    // Constructor
    constructor(map){
        var fuseInput = document.createElement("input");
        $(fuseInput).on("focus", {fuse: this}, this.showPanel);
        $(fuseInput).on("blur", {fuse: this}, this.hidePanel);
        $(fuseInput).on("keyup", {fuse: this}, this.searchFeatures);
        fuseInput.placeholder = "Search...";
        $("#fuse-search").append(fuseInput);
        this._createPanel(map);
        this._map = map;
    }

    //Panel Functions
    _createPanel(){
        var panel = document.createElement("div");
        panel.setAttribute("id", "searchPanel");
        $(panel).hide();
        var results = document.createElement("div");
        results.setAttribute("id", "resultsList");
        $(panel).append(results);
        $("#fuse-search").append(panel);
    }

    showPanel(){
        $("#searchPanel").slideDown();
    }

    hidePanel(){
        $("#searchPanel").slideUp();
    }

    //Search Functions
    indexFeatures(data, keys){
        var jsonFeatures = data.features || data;
        
        this._keys = keys;
        var properties = jsonFeatures.map(function(feature) {
            // Keep track of the original feature
            feature.properties._feature = feature;
            return feature.properties;
        });
        
        var options = {
            keys: keys,
            caseSensitive: false,
            threshold : 0.5
        };
        
        this._fuseIndex = new Fuse(properties, options);
    }

    searchFeatures(event){
        let _self = event.data.fuse;
        console.log(_self);
        var result = _self._fuseIndex.search($("#fuse-search input").val());

        var resultList = document.querySelector('#resultsList');
        resultList.innerHTML = "";
        var num = 0;
        var max = 100;
        for (var i in result) {
            var props = result[i];
            var feature = props._feature;
            
            _self.createResultItem(props, resultList);
            if (undefined !== max && ++num === max)
                break;
        }
    }

    createResultItem(props, container){
        var _self = this;
        var feature = props._feature;

        // Create a container and open the associated popup on click
        var resultItem = document.createElement('p', 'result-item');
        resultItem.dataset.lat = feature.geometry.coordinates[0];
        resultItem.dataset.lng = feature.geometry.coordinates[1];
        
        L.DomUtil.addClass(resultItem, 'clickable');
        resultItem.onclick = function() {
            _self.hidePanel();
            var latlng = [feature.geometry.coordinates[1], feature.geometry.coordinates[0]];
            _self._map.flyTo(latlng, 20)
        };

        // Fill in the container with the user-supplied function if any,
        // otherwise display the feature properties used for the search.
        var str = '<b>' + props[this._keys[0]] + '</b>';
        for (var i = 1; i < this._keys.length; i++) {
            str += '<br/>' + props[this._keys[i]];
        }
        resultItem.innerHTML = str;
        container.append(resultItem);
        return resultItem;
    }
}