class adminForms {
    constructor(appname){
        this.appName = appname;
    }

    showUserRolesForm(){
        // The blanking background
        var popupBg = document.createElement("div");
        $(popupBg).addClass("popupBg");
        $(popupBg).on("click", function(){
            $(this).remove();
        });
        
        // The Form
        var formDiv = document.createElement("div");
        $(formDiv).addClass("popupForm");
        $(formDiv).on("click", function(e){
            e.stopPropagation();
        });

        var form = document.createElement("form");
        form.className = "form";

        var formHeader = document.createElement("div");
        var title = document.createElement("h3");
        title.innerText = "Manage Users";
        formHeader.append(title);
        formDiv.append(formHeader);

        // Quickly wrote a new library for this to reduce clutter
        // and speed up development
        let rolesGrid = new Grid([
            {
                'name': 'name',
                'size': '40%',
            },
            {
                'name': 'email',
                'size': '40%',
            },
            {
                'name': 'role',
                'size': '20%',
            }
        ]);
        
        // Add column headers
        rolesGrid.addCell({
            'start': 'name',
            'end': 'name',
            'content': "Name"
        });

        rolesGrid.addCell({
            'start': 'email',
            'end': 'email',
            'content': "Email Address"
        });

        rolesGrid.addCell({
            'start': 'role',
            'end': 'role',
            'content': "User Role"
        });

        // Retrieve list of users
        $.get('/api/getListOfUsers', function(ret){
            // Add them to the form
            ret.forEach(user => {
                let idHidden = document.createElement("input");
                idHidden.type = "hidden";
                idHidden.name = "users[" + user.id + "][id]";
                idHidden.setAttribute("value", user.id);
                form.append(idHidden);
                
                let emailInput = document.createElement("input");
                emailInput.name = "users[" + user.id + "][email]";
                emailInput.setAttribute("value", user.email);
                
                $.get('/api/getRoles', function(roles){
                    let roleSelect = document.createElement("select");
                    roleSelect.name = "users[" + user.id + "][role]";
                    roles.forEach(role => {
                        let roleOption = document.createElement("option");
                        roleOption.value = role.id;
                        roleOption.innerText = role.role_name;
                        if(role.id == user.role_id){
                            $(roleOption).attr("selected", "selected");
                        }
                        roleSelect.append(roleOption);
                    });

                    rolesGrid.addCell({
                        'start': 'name',
                        'end': 'name',
                        'content': user.name
                    });

                    rolesGrid.addCell({
                        'start': 'email',
                        'end': 'email',
                        'content': emailInput
                    });

                    rolesGrid.addCell({
                        'start': 'role',
                        'end': 'role',
                        'content': roleSelect
                    });
                });
            });

            form.append(rolesGrid.Grid);

            let saveButton = document.createElement("button");
            saveButton.type = "button";
            saveButton.innerHTML = "Save";
            saveButton.className = "button";
            $(saveButton).on("click", function(){
                console.log($(form).serialize());
                $.get('/api/saveRoles/' + $(form).serialize(), function(resp){
                    console.log(resp);
                });
            });

            form.append(saveButton);
        });

        $("body").append(popupBg);
        $(popupBg).append(formDiv);
        $(formDiv).append(form);
    }
}